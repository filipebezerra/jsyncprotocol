package br.com.protocolovirtual.jsyncprotocol.logging;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Este formatador customizado formata partes de um registro de log em uma �nica
 * linha
 * 
 */
class HtmlFormatter extends Formatter {
	/**
	 * Este m�todo � chamado para cada registro de log
	 */
	@Override
	public String format(LogRecord log) {
		StringBuffer buf = new StringBuffer(1000);
		// Bold any levels >= WARNING
		buf.append("<tr>");
		buf.append("<td>");
		if (log.getLevel().intValue() >= Level.WARNING.intValue()) {
			buf.append("<b>");
			buf.append(log.getLevel());
			buf.append("</b>");
		} else {
			buf.append(log.getLevel());
		}
		buf.append("</td>");

		buf.append("<td>");
		buf.append(getFormattedDate(log.getMillis()));
		buf.append("</td>");

		buf.append("<td>");
		buf.append(log.getLoggerName());
		buf.append("</td>");

		buf.append("<td>");
		buf.append(formatMessage(log));
		buf.append("</td>");

		buf.append("</tr>\n");
		return buf.toString();
	}

	private String getFormattedDate(long millisecs) {
		SimpleDateFormat formatter = new SimpleDateFormat(
				"dd 'de' MMM 'de' yyyy '�s' HH:mm:ss", new Locale("pt", "br"));
		Date date = new Date(millisecs);
		return formatter.format(date);
	}

	/**
	 * Este m�todo � chamado apenas depois que o Handler que usa este
	 * formatador, ser criado
	 */
	@Override
	public String getHead(Handler handler) {
		return "<HTML>\n" + "<HEAD>\n"
				+ getFormattedDate(System.currentTimeMillis()) + "\n"
				+ "</HEAD>\n"
				+ "<BODY>\n" + "<PRE>\n" + "<table width=\"100%\" border>\n"
				+ "<tr>" + "<th>Level</th>" + "<th>Time</th>"
				+ "<th>Local</th>" + "<th>Log Message</th>" + "</tr>\n";
	}

	/**
	 * Este m�todo � chamado apenas depois que o Handler que usa este formatador
	 * ser fechado
	 */
	@Override
	public String getTail(Handler handler) {
		return "</table>\n" + "</PRE>" + "</BODY>\n" + "</HTML>\n";
	}
}
