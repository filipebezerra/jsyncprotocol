package br.com.protocolovirtual.jsyncprotocol.logging;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Classe com um �nico m�todo est�tico que define os handlers para os logs de
 * cada classe. Os handles s�o formatos de sa�da dos arquivos de log
 */
public class HandleLogger {
	private static FileHandler fileTextHandler;
	private static SimpleFormatter formatterText;
	private static FileHandler fileHtmlHandler;
	private static Formatter formatterHtml;

	/**
	 * M�todo est�tico que dever� ser chamado em todas classes que implementam
	 * {@link Logger} para definir a sa�da padr�o definida
	 * 
	 * @throws IOException
	 */
	public static void setup() throws IOException {
		Logger logger = Logger.getLogger("");
		logger.setLevel(Level.CONFIG);
		fileTextHandler = new FileHandler("log.txt", true);
		fileHtmlHandler = new FileHandler("log.html", true);

		formatterText = new SimpleFormatter();
		fileTextHandler.setFormatter(formatterText);
		logger.addHandler(fileTextHandler);

		formatterHtml = new HtmlFormatter();
		fileHtmlHandler.setFormatter(formatterHtml);
		logger.addHandler(fileHtmlHandler);
	}
}
