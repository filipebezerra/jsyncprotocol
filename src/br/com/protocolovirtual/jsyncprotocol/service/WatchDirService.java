package br.com.protocolovirtual.jsyncprotocol.service;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import br.com.protocolovirtual.jsyncprotocol.gui.SendProtocolUI;
import br.com.protocolovirtual.jsyncprotocol.logging.HandleLogger;
import br.com.protocolovirtual.jsyncprotocol.util.FileUtilities;
import br.com.protocolovirtual.jsyncprotocol.util.SystemUtilitiesConstants;

/**
 * Classe respons�vel por implementar servi�o que monitora um determinado
 * diret�rio. Entre suas rotinas, h� tamb�m uma que registra sub�rvore de
 * diret�rios � serem monitorados
 * 
 */
public class WatchDirService {

	/**
	 * Membro respons�vel por registrar as mensagens desta classe. Para cada
	 * objeto logger existir� um determinado n�vel para registrar as mensagens,
	 * em quase todos os objetos este n�vel ser� o
	 * java.util.logging.Level.CONFIG
	 */
	private static final Logger LOGGER = Logger.getLogger(WatchDirService.class
			.getCanonicalName());
	/**
	 * 
	 */
	private final WatchService watcher;

	/**
	 * 
	 */
	private Map<WatchKey, Path> keys;

	/**
	 * 
	 */
	private boolean recursive;

	/**
	 * 
	 */
	private boolean trace = false;

	/**
	 * 
	 * @param dir
	 * @param recursive
	 * @throws IOException
	 */
	public WatchDirService(final Path dir, final boolean recursive)
			throws IOException {
		this.watcher = FileSystems.getDefault().newWatchService();
		this.keys = new HashMap<WatchKey, Path>();
		this.recursive = recursive;

		if (recursive) {
			System.out.format("Scanning %s ...\n", dir);
			registerAll(dir);
			System.out.println("Done.");
		} else {
			register(dir);
		}

		LOGGER.setLevel(Level.CONFIG);

		try {
			HandleLogger.setup();
		} catch (IOException e) {
			LOGGER.severe("Attempting setup the logger handlers. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			throw new RuntimeException(
					"Problemas com a cria��o dos arquivos de log do sistema!");
		}

		// enable trace after initial registration
		this.trace = true;
	}

	/**
	 * Rotina respons�vel por registrar o diret�rio dado com a WatchService
	 * 
	 * @param dir
	 * @throws IOException
	 */
	private void register(final Path dir) throws IOException {
		WatchKey key = dir.register(watcher, ENTRY_CREATE);
		if (trace) {
			Path prev = keys.get(key);
			if (prev == null) {
				System.out.format("register: %s\n", dir);
			} else {
				if (!dir.equals(prev)) {
					System.out.format("update: %s -> %s\n", prev, dir);
				}
			}
		}
		keys.put(key, dir);
	}

	/**
	 * Rotina respons�vel por registrar o determinado diret�rio, e todos os seus
	 * sub-diret�rios, com o WatchService WatchService.
	 * 
	 * @param start
	 * @throws IOException
	 */
	private void registerAll(final Path start) throws IOException {
		// register directory and sub-directories
		Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(final Path dir,
					final BasicFileAttributes attrs) throws IOException {
				register(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	/**
	 * 
	 * @param event
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static <T> WatchEvent<T> cast(final WatchEvent<?> event) {
		return (WatchEvent<T>) event;
	}

	/**
	 * 
	 */
	public void processEvents() {
		for (;;) {

			// Aguarda por um novos registros de arquivos criados na pasta
			WatchKey key;
			try {
				key = watcher.take();
			} catch (InterruptedException e) {
				LOGGER.severe("Watching for new events. Error: "
						+ e.getClass().getName() + ": " + e.getMessage());
				return;
			}

			Path dir = keys.get(key);
			if (dir == null) {
				LOGGER.warning("Warn: WatchKey not recognized!!");
				continue;
			}

			for (WatchEvent<?> event : key.pollEvents()) {
				WatchEvent.Kind<?> kind = event.kind();

				// TBD - provide example of how OVERFLOW event is handled
				if (kind == OVERFLOW) {
					LOGGER.warning("Warn: Detected OVERFLOW event - May have been lost or discarted.");
					continue;
				}

				// Context for directory entry event is the file name of entry
				WatchEvent<Path> ev = cast(event);
				Path name = ev.context();
				Path child = dir.resolve(name);

				// imprime o evento + o caminho do arquivo
				System.out.format("%s: %s %s\n", event.kind().name(),
						child.hashCode(), child);

				SendProtocolUI sendProtocolWindow = null;
				JDialog notificationDialog = new JDialog();
				notificationDialog.setModal(false);
				notificationDialog.setAlwaysOnTop(true);
				notificationDialog.setVisible(true);
				notificationDialog.setVisible(false);

				if (child.toFile().isDirectory()) {
					JOptionPane
					.showMessageDialog(
							notificationDialog,
							"Somente arquivos s�o sincronizados. Diret�rios ser�o ignorados!",
							"Entrada inv�lida",
							JOptionPane.WARNING_MESSAGE);
					continue;
				}

				try {
					if (JOptionPane.showConfirmDialog(notificationDialog,
							"Deseja enviar o arquivo "
									+ name.toString().toUpperCase()
									+ " para o sistema?",
									"Um novo protocolo foi criado!!!",
									JOptionPane.YES_NO_OPTION,
									JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
						sendProtocolWindow = new SendProtocolUI();
						sendProtocolWindow.setAttachedField(child.toString());
						sendProtocolWindow.setVisible(true);

						if (sendProtocolWindow.isSentProtocol()) {
							try {
								FileUtilities.moveFile(child.toFile(),
										sendProtocolWindow
										.getTextProtocolNumber()
										.getText());
								JOptionPane.showMessageDialog(
										notificationDialog,
										"Protocolo enviado com sucesso!!!",
										"Envio de Protocolo",
										JOptionPane.INFORMATION_MESSAGE);
							} catch (IOException e) {
								LOGGER.severe("Attempting move a sent protocol. Error: "
										+ e.getClass().getName()
										+ ": "
										+ e.getMessage());
								JOptionPane
								.showMessageDialog(
										notificationDialog,
										"Falha ao tentar mover o protocolo enviado para a pasta '"
												+ SystemUtilitiesConstants.DIR_NAME_PROTOCOLS_SENT
												+ "'.",
												"Falha ao mover arquivo",
												JOptionPane.ERROR_MESSAGE);
							}
						}
					} else {
						try {
							FileUtilities.moveFile(child.toFile(), null);
						} catch (IOException e) {
							LOGGER.severe("Attempting move a not sent protocol. Error: "
									+ e.getClass().getName()
									+ ": "
									+ e.getMessage());
							JOptionPane
							.showMessageDialog(
									notificationDialog,
									"Falha ao tentar mover o protocolo n�o enviado para a pasta '"
											+ SystemUtilitiesConstants.DIR_NAME_PROTOCOLS_NOT_SENT
											+ "'.",
											"Falha ao mover arquivo",
											JOptionPane.ERROR_MESSAGE);
						}
					}
				} finally {
					if (notificationDialog != null
							&& notificationDialog.isEnabled()) {
						notificationDialog.dispose();
					}

					if (sendProtocolWindow != null
							&& sendProtocolWindow.isEnabled()) {
						sendProtocolWindow.dispose();
					}
				}
				// if directory is created, and watching recursively, then
				// register it and its sub-directories
				if (recursive && kind == ENTRY_CREATE) {
					try {
						if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
							registerAll(child);
						}
					} catch (IOException x) {
						// ignore to keep sample readbale
					}
				}
			}

			// reset key and remove from set if directory no longer accessible
			boolean valid = key.reset();
			if (!valid) {
				keys.remove(key);

				// all directories are inaccessible
				if (keys.isEmpty()) {
					break;
				}
			}
		}
	}
}
