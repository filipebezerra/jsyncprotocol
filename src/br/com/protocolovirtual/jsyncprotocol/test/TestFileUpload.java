package br.com.protocolovirtual.jsyncprotocol.test;

import java.io.IOException;

import org.junit.Test;

import br.com.protocolovirtual.jsyncprotocol.util.SystemUtilitiesMethods;

/**
 * Caso de teste JUnit para upload de arquivo via protocolo SSH
 * 
 */
public class TestFileUpload {

	/**
	 * M�todo de teste que executa upload de um arquivo txt que est� no
	 * classpath no servidor de destino utilizando o protocolo SSH
	 */
	@Test
	public void testDoUploadFileToFileServer() {
		try {
			SystemUtilitiesMethods.doUploadFileToFileServer(
					"protocolovirtual.com.br", "armazenanfe", "Y6tr145C", null,
					"absprotocolo/protocolo/admin/protocolos/arquivos", null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
