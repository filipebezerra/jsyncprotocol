package br.com.protocolovirtual.jsyncprotocol.model;

/**
 * Classe modelo para os dados de remetente
 */
public class UserSender {
	/**
	 * C�digo �nico do remetente
	 */
	private long id;

	/**
	 * Nome do usu�rio logado
	 */
	private String name;

	/**
	 * Nome de usu�rio do remetente
	 */
	private String userName;

	/**
	 * Escrit�rio do usu�rio logado
	 */
	private Office office;

	/**
	 * Construtor padr�o da classe
	 */
	public UserSender() {
		super();
	}

	/**
	 * Construtor com argumentos para os membros da inst�ncia
	 * 
	 * @param id
	 *            c�digo �nico do remetente
	 * @param userName
	 *            nome de usu�rio do remetente
	 * @param officeId
	 *            c�digo do escrit�rio do remetente
	 */
	public UserSender(final long id, final String userName, final Office office) {
		super();
		this.id = id;
		this.userName = userName;
		this.office = office;
	}

	/**
	 * Retorna o c�digo �nico do remetente que o identifica no banco de dados
	 * 
	 * @return o c�digo �nico do remetente
	 */
	public long getId() {
		return id;
	}

	/**
	 * Armazena como c�digo �nico do remetente que o identifica no banco de
	 * dados
	 * 
	 * @param id
	 *            o c�digo �nico do remetente
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Retorna o nome do usu�rio logado
	 * 
	 * @return o nome do usu�rio
	 */
	public String getName() {
		return name;
	}

	/**
	 * Armazena o nome do usu�rio logado
	 * 
	 * @param name
	 *            o nome do usu�rio
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Retorna o nome de usu�rio do remetente
	 * 
	 * @return o nome de usu�rio do remetente
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Armazena o nome de usu�rio do remetente
	 * 
	 * @param userName
	 *            nome de usu�rio do remetente
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Retorna o escrit�rio do remetente
	 * 
	 * @return o escrit�rio do remetente
	 */
	public Office getOffice() {
		return office;
	}

	/**
	 * Armazena o escrit�rio do remetente
	 * 
	 * @param office
	 *            o escrit�rio do remetente
	 */
	public void setOffice(Office office) {
		this.office = office;
	}

	/**
	 * Retorna o nome que representa o remetente
	 */
	@Override
	public String toString() {
		return userName;
	}

	/**
	 * Retorna hashcode baseado no c�digo �nico
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ id >>> 32);
		return result;
	}

	/**
	 * Retorna resultado da compara��o entre duas inst�ncias do mesmo tipo
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UserSender))
			return false;
		UserSender other = (UserSender) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
