package br.com.protocolovirtual.jsyncprotocol.model;

/**
 * Classe modelo para os dados de destinat�rio
 */
public class UserReceiver {
	/**
	 * C�digo �nico do destinat�rio
	 */
	private long id;

	/**
	 * Nome do destinat�rio
	 */
	private String name;

	/**
	 * Construtor padr�o da classe
	 */
	public UserReceiver() {
		super();
	}

	/**
	 * Construtor com argumentos para os membros da inst�ncia
	 * 
	 * @param id
	 *            c�digo �nico do destinat�rio
	 * @param name
	 *            nome do destinat�rio
	 */
	public UserReceiver(final long id, final String fullName) {
		super();
		this.id = id;
		this.name = fullName;
	}

	/**
	 * Retorna o c�digo �nico do destinat�rio que o identifica no banco de dados
	 * 
	 * @return o c�digo �nico do destinat�rio
	 */
	public long getId() {
		return id;
	}

	/**
	 * Armazena como c�digo �nico do destinat�rio que o identifica no banco de
	 * dados
	 * 
	 * @param id
	 *            o c�digo �nico do destinat�rio
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Retorna o nome do destinat�rio
	 * 
	 * @return o nome do destinat�rio
	 */
	public String getName() {
		return name;
	}

	/**
	 * Armazena o nome do destinat�rio
	 * 
	 * @param name
	 *            o nome do destinat�rio
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Retorna o nome que representa o destinat�rio
	 */
	@Override
	public String toString() {
		return name;
	}

	/**
	 * Retorna hashcode baseado no c�digo �nico
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ id >>> 32);
		return result;
	}

	/**
	 * Retorna resultado da compara��o entre duas inst�ncias do mesmo tipo
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UserReceiver))
			return false;
		UserReceiver other = (UserReceiver) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
