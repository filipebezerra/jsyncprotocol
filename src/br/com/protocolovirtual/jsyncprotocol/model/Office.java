package br.com.protocolovirtual.jsyncprotocol.model;

/**
 * Classe modelo para o escrit�rio do usu�rio logado no sistema, que tamb�m � o
 * remetente ao envio dos protocolos
 */
public class Office {
	/**
	 * C�digo �nico do escrit�rio
	 */
	private int id;

	/**
	 * Nome do escrit�rio
	 */
	private String name;

	/**
	 * Construtor padr�o da classe
	 */
	public Office() {
		super();
	}

	/**
	 * Construtor com argumentos para os membros da inst�ncia
	 * 
	 * @param id
	 *            o c�digo do escrit�rio
	 * @param name
	 *            o nome do escrit�rio
	 */
	public Office(final int id, final String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Retorna o c�digo do escrit�rio
	 * 
	 * @return o c�digo do escrit�rio
	 */
	public int getId() {
		return id;
	}

	/**
	 * Armazena o c�digo do escrit�rio
	 * 
	 * @param id
	 *            o c�digo do escrit�rio
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retorna o nome do escrit�rio
	 * 
	 * @return name o nome do escrit�rio
	 */
	public String getName() {
		return name;
	}

	/**
	 * Armazena o nome do escrit�rio
	 * 
	 * @param name
	 *            o nome do escrit�rio
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	/**
	 * Retorna hashcode baseado no c�digo �nico
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/**
	 * Retorna resultado da compara��o entre duas inst�ncias do mesmo tipo
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Office other = (Office) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
