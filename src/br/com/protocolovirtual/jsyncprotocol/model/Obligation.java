package br.com.protocolovirtual.jsyncprotocol.model;

/**
 * Classe modelo para os dados de obriga��es
 */
public class Obligation {
	/**
	 * C�digo �nico da obriga��o
	 */
	private long id;

	/**
	 * Nome da obriga��o
	 */
	private String name;

	/**
	 * Construtor padr�o da classe
	 */
	public Obligation() {
		super();
	}

	/**
	 * Construtor com argumentos para os membros da inst�ncia
	 * 
	 * @param id
	 *            c�digo �nico da obriga��o
	 * @param name
	 *            nome da obriga��o
	 */
	public Obligation(final long id, final String name) {
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * Retorna o c�digo �nico da obriga��o que a identifica no banco de dados
	 * 
	 * @return o c�digo �nico da obriga��o
	 */
	public long getId() {
		return id;
	}

	/**
	 * Armazena como c�digo �nico da obriga��o que a identifica no banco de
	 * dados
	 * 
	 * @param id
	 *            o c�digo �nico da obriga��o
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Retorna o nome da obriga��o
	 * 
	 * @return o nome da obriga��o
	 */
	public String getName() {
		return name;
	}

	/**
	 * Armazena o nome da obriga��o
	 * 
	 * @param name
	 *            o nome da obriga��o
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Retorna o nome que representa a obriga��o
	 */
	@Override
	public String toString() {
		return name;
	}

	/**
	 * Retorna hashcode baseado no c�digo �nico
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ id >>> 32);
		return result;
	}

	/**
	 * Retorna resultado da compara��o entre duas inst�ncias do mesmo tipo
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Obligation))
			return false;
		Obligation other = (Obligation) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
