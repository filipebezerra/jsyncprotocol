package br.com.protocolovirtual.jsyncprotocol.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import br.com.protocolovirtual.jsyncprotocol.dao.DatabasePersistenceDao;
import br.com.protocolovirtual.jsyncprotocol.dao.FilePersistenceDao;
import br.com.protocolovirtual.jsyncprotocol.logging.HandleLogger;
import br.com.protocolovirtual.jsyncprotocol.model.Obligation;
import br.com.protocolovirtual.jsyncprotocol.model.Office;
import br.com.protocolovirtual.jsyncprotocol.model.UserReceiver;
import br.com.protocolovirtual.jsyncprotocol.model.UserSender;
import br.com.protocolovirtual.jsyncprotocol.service.WatchDirService;
import br.com.protocolovirtual.jsyncprotocol.util.DateTimeUtilities;
import br.com.protocolovirtual.jsyncprotocol.util.SystemUtilitiesMethods;

/**
 * Classe que cont�m uma interface gr�fica e representa os dados que ser�o
 * inseridos no servidor de banco de dados e o protocolo que ser� sincronizado.
 */
public class SendProtocolUI extends JDialog implements WindowListener {

	/*
	 * Class Constants
	 */
	/**
	 * Membro respons�vel por registrar as mensagens desta classe. Para cada
	 * objeto logger existir� um determinado n�vel para registrar as mensagens,
	 * em quase todos os objetos este n�vel ser� o
	 * java.util.logging.Level.CONFIG.
	 */
	private static final Logger LOGGER = Logger.getLogger(WatchDirService.class
			.getCanonicalName());

	/**
	 * ID de identifica��o da vers�o da classe para manter controle da evolu��o
	 * 
	 * @category Serializa��o de objetos
	 * 
	 * @see {@link http www.javapractices.com/topic/TopicAction.do?Id=45} ou
	 * @see {@link http blog.caelum.com.br/entendendo-o-serialversionuid/}
	 * @see {@link http
	 *      docs.oracle.com/javase/1.4.2/docs/api/java/io/Serializable.html}
	 * 
	 */
	private static final long serialVersionUID = 1296893950209305L;

	/**
	 * Contante que armazena a varia��o (range) que ser� gerado aleatoriamente
	 * para compor o n�mero do protocolo.
	 */
	private static final int DEFAULT_INT_RANDOMIZED = 100;

	/**
	 * Constante que armazena o tipo do protocolo que ser� enviado. Este campo
	 * ser� registrado tamb�m em um registro na tabela 'protocolos'.
	 */
	private static final String PROTOCOL_TYPE = "Via Programa";

	/**
	 * Constante que armazena a posi��o horizontal padr�o dos componentes
	 * gr�ficos representados no formul�rio. � utilizado para fornecer aux�lio
	 * no m�todo {@link #setLocationRelativeToComponent(Component, Component)}.
	 */
	private static final int DEFAULT_POS_X = 88;

	/**
	 * Constante que armazena a posi��o vertical relativa ao componente base. �
	 * utilizado para fornecer aux�lio no m�todo
	 * {@link #setLocationRelativeToComponent(Component, Component)}.
	 */
	private static final int DEFAULT_RELATIVE_POS_Y = 10;

	/*
	 * Instance Fields
	 */
	/**
	 * Campo gr�fico para o texto de descri��o do protocolo.
	 */
	private JTextArea textDescription;

	/**
	 * Campo gr�fico para a lista de usu�rios destinat�rios.
	 */
	private JComboBox<UserReceiver> comboBoxReceiver;

	/**
	 * Campo gr�fico para a lista de obriga��es.
	 */
	private JComboBox<Obligation> comboBoxObligations;

	/**
	 * Campo gr�fico para a numera��o do protocolo gerada automaticamente.
	 */
	private JTextField textProtocolNumber;

	/**
	 * Campo gr�fico para o arquivo de protocolo anexado.
	 */
	private JTextField textAttached;

	/**
	 * Campo gr�fico para o nome do escrit�rio para o usu�rio logado.
	 */
	private JTextField textUserOffice;

	/**
	 * Campo gr�fico para o nome do usu�rio logado.
	 */
	private JTextField textUserRealName;

	/**
	 * Campo gr�fico para o t�tulo do campo de anexo.
	 */
	private JLabel lblAttached;

	/**
	 * Campo gr�fico para o t�tulo do campo de n�mero do protocolo.
	 */
	private JLabel lblProtocolNumber;

	/**
	 * Campo gr�fico para o t�tulo do campo da lista de destinat�rios.
	 */
	private JLabel lblReceiver;

	/**
	 * Campo gr�fico para o t�tulo do campo da lista de obriga��es.
	 */
	private JLabel lblObligation;

	/**
	 * Campo gr�fico para o t�tulo do campo de descri��o do protocolo.
	 */
	private JLabel lblDescription;

	/**
	 * Campo gr�fico para o t�tulo do campo do nome do usu�rio logado.
	 */
	private JLabel lblUserRealName;

	/**
	 * Campo gr�fico para o t�tulo do campo do nome do escrit�rio do usu�rio
	 * logado.
	 */
	private JLabel lblUserOffice;

	/**
	 * Campo gr�fico para o texto est�tico de campos que s�o requeridos seu
	 * preenchimento.
	 */
	private JLabel lblRequiredFields;

	/**
	 * Campo gr�fico para o bot�o de a��o de enviar o protocolo.
	 */
	private JButton btnSend;

	/**
	 * Campo gr�fico para o bot�o de a��o de recarregar lista de destinat�rios.
	 */
	private JButton btnReloadReceiverList;

	/*
	 * Instance Attributes
	 */
	/**
	 * Date de registro/cadastro do protocolo.
	 */
	private String registrationDate;

	/**
	 * Usu�rio respons�vel pelo envio/remetente que est� logado no sistema.
	 */
	private UserSender sender;

	/**
	 * Tipo do protocolo.
	 */
	private String protocolType;

	/**
	 * Reporta se o protocolo em quest�o foi enviado com sucesso.
	 */
	private boolean sentProtocol;


	/**
	 * Construtor respons�vel por inicializar vari�veis de inst�ncia, organizar
	 * o layout do formul�rio, inicializar componentes gr�ficos, adicionar
	 * listeners para o formul�rio, carregar campos autom�ticos e configurar o
	 * gerenciador de log.
	 */
	public SendProtocolUI() {
		setTitle("Preencha os dados para enviar o protocolo");
		setName("frameSendProtocol");
		getContentPane().setFont(new Font("Verdana", Font.PLAIN, 10));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 525);
		setPreferredSize(new Dimension(450, 525));
		setLocationRelativeTo(this);
		getContentPane().setLayout(null);
		setResizable(false);
		setModal(true);
		setAlwaysOnTop(true);
		setUpComponents();
		setAutomaticValuesToFields();
		addWindowListener(this);
		pack();
		setupLogger();
	}

	/**
	 * M�todo respons�vel por inicializar e configurar o gerenciador de log
	 * deste formul�rio.
	 */
	private void setupLogger() {
		LOGGER.setLevel(Level.CONFIG);

		try {
			HandleLogger.setup();
		} catch (IOException e) {
			LOGGER.severe("Attempting setup the logger handlers. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					this,
					"Problemas com a cria��o dos arquivos de log do sistema!",
					"Configura��o de log do sistema",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Retorna a data de registro/cadastro do protocolo. Este valor � gerado
	 * automaticamente na cria��o do formul�rio.
	 * 
	 * @return a data de registro/cadastro do protocolo.
	 */
	public String getRegistrationDate() {
		return registrationDate;
	}

	/**
	 * Retorna o usu�rio remetente que est� logado no sistema e � respons�vel
	 * pelo envio dos protocolos.
	 * 
	 * @return o usu�rio remetente do protocolo.
	 */
	public UserSender getSender() {
		return sender;
	}

	/**
	 * Retorna o tipo do protocolo a ser enviado.
	 * 
	 * @return o tipo do protocolo.
	 */
	public String getProtocolType() {
		return protocolType;
	}

	/**
	 * Retorna se o protolo em quest�o foi devidamente encaminhado e registrado.
	 * 
	 * @return <b>true</b> se foi enviado ou <b>false</b> caso houveram falhas.
	 */
	public boolean isSentProtocol() {
		return sentProtocol;
	}

	/**
	 * Retorna o campo gr�fico respons�vel por armazenar e renderizar o caminho
	 * absoluto do protocolo anexado.
	 * 
	 * @return o campo do caminho absoluto do protocolo.
	 */
	public JTextField getTextAttached() {
		return textAttached;
	}

	/**
	 * Armazena o caminho absoluto do protocolo no campo gr�fico.
	 * 
	 * @param attachedPath
	 *            o caminho absoluto do arquivo protocolo.
	 */
	public void setAttachedField(final String attachedPath) {
		textAttached.setText(attachedPath);
	}

	/**
	 * Retorna o campo gr�fico respons�vel por armazenar e renderizar o n�mero
	 * do protocolo gerado automaticamente na cria��o da formul�rio.
	 * 
	 * @return o campo que armazena o n�mero do protocolo gerado.
	 */
	public JTextField getTextProtocolNumber() {
		return textProtocolNumber;
	}

	/**
	 * M�todo respons�vel por criar, carregar, dimensionar, posicionar,
	 * adicionar listeners, criar teclas de atalhos e adicionar todos
	 * componentes gr�ficos como campos de texto {@link JTextField} ou
	 * {@link JTextArea} e listas {@link JComboBox} ao formul�rio.
	 */
	private void setUpComponents() {
		lblProtocolNumber = new JLabel("N\u00FAmero do Protocolo");
		lblProtocolNumber.setBounds(new Rectangle(0, 0, 26, 0));
		lblProtocolNumber.setFont(new Font("Verdana", Font.BOLD, 10));
		lblProtocolNumber.setBounds(88, 141, 141, 14);
		getContentPane().add(lblProtocolNumber);

		textProtocolNumber = new JTextField();
		textProtocolNumber
		.setToolTipText("Numera\u00E7\u00E3o \u00FAnica que identifica o protocolo gerada dinamicamente.");
		textProtocolNumber.setEditable(false);
		textProtocolNumber.setColumns(10);
		textProtocolNumber.setBackground(SystemColor.controlHighlight);
		textProtocolNumber.setBounds(88, 156, 135, 26);
		getContentPane().add(textProtocolNumber);
		lblProtocolNumber.setLabelFor(textProtocolNumber);
		lblProtocolNumber.setDisplayedMnemonic(KeyEvent.VK_P);

		lblReceiver = new JLabel("Destinat\u00E1rio");
		lblReceiver.setFont(new Font("Verdana", Font.BOLD, 10));
		lblReceiver.setBounds(new Rectangle(0, 0, 26, 0));
		lblReceiver.setBounds(88, 191, 100, 14);
		getContentPane().add(lblReceiver);

		comboBoxReceiver = new JComboBox<>();
		comboBoxReceiver.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		comboBoxReceiver
		.setToolTipText("Usu\u00E1rio que ser\u00E1 notificado do protocolo enviado.");
		comboBoxReceiver.setBackground(Color.WHITE);
		comboBoxReceiver.setCursor(Cursor
				.getPredefinedCursor(Cursor.HAND_CURSOR));
		comboBoxReceiver.setBounds(new Rectangle(0, 0, 26, 0));
		comboBoxReceiver.setFont(new Font("Verdana", Font.PLAIN, 10));
		comboBoxReceiver.setBounds(88, 206, 270, 26);
		getContentPane().add(comboBoxReceiver);
		lblReceiver.setLabelFor(comboBoxReceiver);
		lblReceiver.setDisplayedMnemonic(KeyEvent.VK_N);

		lblObligation = new JLabel("Obriga\u00E7\u00E3o");
		lblObligation.setFont(new Font("Verdana", Font.BOLD, 10));
		lblObligation.setBounds(new Rectangle(0, 0, 26, 0));
		lblObligation.setBounds(88, 241, 100, 14);
		getContentPane().add(lblObligation);

		comboBoxObligations = new JComboBox<>();
		comboBoxObligations.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		comboBoxObligations
		.setToolTipText("Obriga\u00E7\u00E3o referente ao protocolo.");
		comboBoxObligations.setBackground(Color.WHITE);
		comboBoxObligations.setCursor(Cursor
				.getPredefinedCursor(Cursor.HAND_CURSOR));
		comboBoxObligations.setBounds(new Rectangle(0, 0, 26, 0));
		comboBoxObligations.setFont(new Font("Verdana", Font.PLAIN, 10));
		comboBoxObligations.setBounds(88, 256, 270, 26);
		getContentPane().add(comboBoxObligations);
		lblObligation.setLabelFor(comboBoxObligations);
		lblObligation.setDisplayedMnemonic(KeyEvent.VK_O);

		lblDescription = new JLabel("Descri\u00E7\u00E3o");
		lblDescription.setFont(new Font("Verdana", Font.BOLD, 10));
		lblDescription.setBounds(new Rectangle(0, 0, 26, 0));
		lblDescription.setBounds(88, 291, 100, 14);
		getContentPane().add(lblDescription);

		textDescription = new JTextArea();
		textDescription
		.setToolTipText("Descri\u00E7\u00E3o simples do protocolo \u00E0 ser sincronizado.");
		textDescription.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_TAB) {
					textAttached.requestFocusInWindow();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		textDescription.setBounds(new Rectangle(0, 0, 26, 0));
		textDescription.setFont(new Font("Verdana", Font.PLAIN, 10));
		textDescription.setBounds(88, 406, 270, 78);
		textDescription.setTabSize(0);
		textDescription.setLineWrap(true);
		textDescription.setWrapStyleWord(true);
		lblDescription.setLabelFor(textDescription);
		lblDescription.setDisplayedMnemonic(KeyEvent.VK_S);

		JScrollPane scroll = new JScrollPane(textDescription);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setBounds(new Rectangle(88, 306, 270, 78));
		getContentPane().add(scroll);

		lblAttached = new JLabel("Anexo");
		lblAttached.setBounds(new Rectangle(0, 0, 26, 0));
		lblAttached.setFont(new Font("Verdana", Font.BOLD, 10));
		lblAttached.setBounds(88, 393, 46, 14);
		getContentPane().add(lblAttached);

		textAttached = new JTextField();
		textAttached
		.setToolTipText("Protocolo anexado ao envio e que ser\u00E1 sincronizado.");
		textAttached.setEditable(false);
		textAttached.setColumns(10);
		textAttached.setBackground(SystemColor.controlHighlight);
		textAttached.setBounds(88, 408, 270, 26);
		getContentPane().add(textAttached);
		lblAttached.setLabelFor(textAttached);
		lblAttached.setDisplayedMnemonic(KeyEvent.VK_A);

		lblRequiredFields = new JLabel("Todos os campos s\u00E3o requeridos!");
		lblRequiredFields.setVisible(false);
		lblRequiredFields.setFont(new Font("Verdana", Font.BOLD, 12));
		lblRequiredFields.setForeground(Color.RED);
		lblRequiredFields.setBounds(88, 20, 270, 14);
		getContentPane().add(lblRequiredFields);

		lblUserRealName = new JLabel("Nome Usu\u00E1rio");
		lblUserRealName.setFont(new Font("Verdana", Font.BOLD, 10));
		lblUserRealName.setBounds(88, 91, 100, 16);
		getContentPane().add(lblUserRealName);

		textUserRealName = new JTextField();
		textUserRealName
		.setToolTipText("Numera\u00E7\u00E3o \u00FAnica que identifica o protocolo gerada dinamicamente.");
		textUserRealName.setText((String) null);
		textUserRealName.setEditable(false);
		textUserRealName.setColumns(10);
		textUserRealName.setBackground(SystemColor.controlHighlight);
		textUserRealName.setBounds(88, 106, 270, 26);
		getContentPane().add(textUserRealName);
		lblUserRealName.setLabelFor(textUserRealName);
		lblUserRealName.setDisplayedMnemonic(KeyEvent.VK_U);

		lblUserOffice = new JLabel("Escrit\u00F3rio");
		lblUserOffice.setFont(new Font("Verdana", Font.BOLD, 10));
		lblUserOffice.setBounds(88, 41, 55, 16);
		getContentPane().add(lblUserOffice);

		textUserOffice = new JTextField();
		textUserOffice
		.setToolTipText("Numera\u00E7\u00E3o \u00FAnica que identifica o protocolo gerada dinamicamente.");
		textUserOffice.setText((String) null);
		textUserOffice.setEditable(false);
		textUserOffice.setColumns(10);
		textUserOffice.setBackground(SystemColor.controlHighlight);
		textUserOffice.setBounds(88, 56, 270, 26);
		getContentPane().add(textUserOffice);
		lblUserOffice.setLabelFor(textUserOffice);
		lblUserOffice.setDisplayedMnemonic(KeyEvent.VK_C);

		btnSend = new JButton("Enviar");
		btnSend.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					doSendProtocol();
				}
			}
		});
		btnSend.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doSendProtocol();
			}
		});
		btnSend.setBounds(new Rectangle(0, 0, 26, 0));
		btnSend.setFont(new Font("Verdana", Font.PLAIN, 10));
		btnSend.setToolTipText("Clique aqui para encaminhar o protocolo ao destinat\u00E1rio.");
		btnSend.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSend.setBounds(268, 446, 90, 26);
		btnSend.setMnemonic(KeyEvent.VK_E);
		btnSend.setDisplayedMnemonicIndex(0);
		getContentPane().add(btnSend);

		btnReloadReceiverList = new JButton(
				"Atualizar Destinat\u00E1rios");
		btnReloadReceiverList.setCursor(Cursor
				.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnReloadReceiverList.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					getReceiverList();
				}
			}
		});
		btnReloadReceiverList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getReceiverList();
			}
		});
		btnReloadReceiverList
		.setToolTipText("Clique aqui para recarregar a lista de destinat\u00E1rios.");
		btnReloadReceiverList.setMnemonic(KeyEvent.VK_E);
		btnReloadReceiverList.setFont(new Font("Verdana", Font.PLAIN, 10));
		btnReloadReceiverList.setDisplayedMnemonicIndex(0);
		btnReloadReceiverList.setBounds(new Rectangle(0, 0, 26, 0));
		btnReloadReceiverList.setBounds(98, 444, 155, 26);
		btnReloadReceiverList.setMnemonic(KeyEvent.VK_D);
		btnReloadReceiverList.setDisplayedMnemonicIndex(10);
		getContentPane().add(btnReloadReceiverList);

		setLocationRelativeToComponent(textAttached, btnSend);
	}

	/**
	 * M�todo respons�vel por exibir a label da mensagem dos campos que s�o
	 * requeridos o preenchimento manual.
	 */
	public void setRequiredFieldsLabelUnvisible() {
		if (lblRequiredFields.isVisible()) {
			lblRequiredFields.setVisible(false);
		}
	}

	/**
	 * M�todo respons�vel por verificar se todos os campos que requerem
	 * preechimento manual, est�o devidamente preenchidos.
	 * 
	 * @return <b>true</b> se todos os campos est�o preenchidos corretamente ou
	 *         <b>false</b> se caso qualquer um dos campos requeridos n�o
	 *         estiverem preenchidos.
	 */
	private boolean checkRequiredFields() {
		Object fieldObligation = comboBoxObligations.getSelectedItem();
		Object fieldReceiver = comboBoxReceiver.getSelectedItem();
		String fieldDescription = textDescription.getText().trim();

		if (fieldObligation == null || fieldReceiver == null
				|| fieldDescription == null || fieldDescription.isEmpty()) {
			lblRequiredFields.setVisible(true);
			return false;
		}

		return true;
	}

	/**
	 * M�todo respons�vel por randomizar uma sequ�ncia num�rica de n�meros
	 * inteiros e gerar um n�mero baseado na varia��o padr�o (
	 * {@link #DEFAULT_INT_RANDOMIZED}).
	 * 
	 * @return o n�mero inteiro gerado dentro da varia��o padr�o.
	 */
	private int randomInt() {
		Random random = new Random();
		return random.nextInt(DEFAULT_INT_RANDOMIZED);
	}

	/**
	 * M�todo respons�vel por gerar e formar a numera��o do protocolo. O padr�o
	 * definido � composto por: <br>
	 * <b>2 casas</b> decimais para a <b>hora corrente</b><br>
	 * <b>2 casas</b> decimais para o <b>minuto da hora corrente</b><br>
	 * <b>2 casas</b> decimais para o <b>segundo do minuto da hora corrente</b><br>
	 * <b>6 casas</b> decimais para 2 <b>n�meros gerados aleatoriamente</b> (
	 * {@link #randomInt()})<br>
	 * <b>1 ou 2 casas</b> decimais para o <b>c�digo do usu�rio logado</b>.
	 * 
	 * @param date
	 *            a data corrente do sistema.
	 * @param loggedUserId
	 *            o c�digo do usu�rio logado no sistema.
	 * @return o n�mero do protocolo.
	 */
	private String getProtocolNumber(final Date date, final long loggedUserId) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		StringBuffer buffer = new StringBuffer();
		buffer.append(SystemUtilitiesMethods.formatCharAmountOf(
				String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)), 2, '0',
				true));
		buffer.append(SystemUtilitiesMethods.formatCharAmountOf(
				String.valueOf(calendar.get(Calendar.MINUTE)), 2, '0', true));
		buffer.append(SystemUtilitiesMethods.formatCharAmountOf(
				String.valueOf(calendar.get(Calendar.SECOND)), 2, '0', true));
		buffer.append(SystemUtilitiesMethods.formatCharAmountOf(
				String.valueOf(randomInt()), 3, '0', true));
		buffer.append(SystemUtilitiesMethods.formatCharAmountOf(
				String.valueOf(randomInt()), 3, '0', true));
		buffer.append(loggedUserId);
		return buffer.toString();
	}

	/**
	 * M�todo respons�vel por carregar e preencher a lista dos destinat�rios.
	 */
	private void getReceiverList() {
		SystemUtilitiesMethods.setWaitProcessCursor(this);
		DatabasePersistenceDao databasePersistenceDao = new DatabasePersistenceDao();
		try {
			comboBoxReceiver.removeAllItems();
			for (UserReceiver userReceiver : databasePersistenceDao
					.retrieveUserReceiver(sender.getOffice().getId())) {
				comboBoxReceiver.addItem(userReceiver);
			}
		} catch (GeneralSecurityException e) {
			// TODO Tratar exce��o e Logger
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Tratar exce��o e Logger
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Tratar exce��o e Logger
			e.printStackTrace();
		} catch (SQLException e) {
			LOGGER.severe("Attempting to retrieve the receivers list into combobox component. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					this,
					"Ocorreu uma falha ao tentar comunicar com servidor de banco de dados.\nCausa do erro: "
							+ e.getClass().toString()
							+ ": "
							+ e.getMessage(),
							"Erro ao comunicar com servidor",
							JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			SystemUtilitiesMethods.setDefaultCursor(this);
		}
	}

	/**
	 * M�todo respons�vel por carregar e preencher a lista das obriga��es.
	 */
	private void getObligationList() {
		SystemUtilitiesMethods.setWaitProcessCursor(this);
		DatabasePersistenceDao databasePersistenceDao = new DatabasePersistenceDao();
		try {
			comboBoxObligations.removeAllItems();
			for (Obligation fieldObrigacao : databasePersistenceDao
					.retrieveFieldObrigacao(sender.getOffice().getId())) {
				comboBoxObligations.addItem(fieldObrigacao);
			}
		} catch (GeneralSecurityException e) {
			// TODO Tratar exce��o e Logger
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Tratar exce��o e Logger
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Tratar exce��o e Logger
			e.printStackTrace();
		} catch (SQLException e) {
			LOGGER.severe("Attempting to retrieve the obligations list into combobox component. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					this,
					"Ocorreu uma falha ao tentar comunicar com servidor de banco de dados.\nCausa do erro: "
							+ e.getClass().toString()
							+ ": "
							+ e.getMessage(),
							"Erro ao comunicar com servidor",
							JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			SystemUtilitiesMethods.setDefaultCursor(this);
		}
	}

	/**
	 * M�todo respons�vel por carregar as propriedades do usu�rio logado no
	 * sistema e armazen�-las na vari�vel de inst�ncia que referencia um usu�rio
	 * remetente ({@link #sender}).
	 */
	private void getPropertiesFileValuesToField() {
		FilePersistenceDao filePersistenceDao = new FilePersistenceDao();
		try {
			sender = new UserSender();
			Properties propertiesValues = filePersistenceDao
					.getSystemPropertiesFile();
			sender.setId(Integer.valueOf(propertiesValues
					.getProperty("logged_userid")));
			sender.setName(propertiesValues.getProperty("logged_realname"));
			sender.setOffice(new Office(Integer.parseInt(propertiesValues
					.getProperty("logged_officeid")), propertiesValues
					.getProperty("logged_officename")));
			sender.setUserName(propertiesValues.getProperty("logged_username"));
		} catch (FileNotFoundException e) {
			LOGGER.severe("System Properties not found attempting to get system properties file. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					this,
					"N�o foi poss�vel localizar o arquivo de propriedades para configurar o sistema!",
					"Configura��o do Sistema",
					JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			LOGGER.severe("Output input error attempting to get system properties file. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					this,
					"N�o foi poss�vel acessar o arquivo de propriedades para configurar o sistema!",
					"Configura��o do Sistema",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * M�todo respons�vel por inicializar e preencher os valores dos campos
	 * gerados automaticamente na cria��o do formul�rio.
	 */
	private void setAutomaticValuesToFields() {
		Date dateNow = new Date();
		registrationDate = DateTimeUtilities.getDateAsString(dateNow);
		protocolType = PROTOCOL_TYPE;
		getPropertiesFileValuesToField();
		textUserOffice.setText(sender.getOffice().getName());
		textUserRealName.setText(sender.getName());
		textProtocolNumber.setText(getProtocolNumber(dateNow, sender.getId()));
		getReceiverList();
		getObligationList();
	}

	/**
	 * M�todo principal do formul�rio, respons�vel por encaminhar o protocolo em
	 * anexo utilizando os campos gr�ficos devidamente preechidos com as
	 * informa��es necess�rias para ser registrado no banco de dados. Nesta
	 * rotina � executado o envio do arquivo protocolo para o servidor de
	 * arquivamento (via SSH) e registra o envio na tabela <b>protocolos</b> no
	 * banco de dados do servidor.
	 */
	private void doSendProtocol() {
		if (checkRequiredFields() == false)
			return;

		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		SystemUtilitiesMethods.setWaitProcessCursor(this);
		btnSend.setEnabled(false);

		try {

			FilePersistenceDao filePersistenceDao = new FilePersistenceDao();
			Properties authPropertiesFile;
			String sshHost = null, sshUser = null, sshPw = null, sshDestinationDir = null;
			try {
				authPropertiesFile = filePersistenceDao
						.getAuthPropertiesDecryptedFile();
				sshHost = authPropertiesFile.getProperty("srvarc_host");
				sshUser = authPropertiesFile.getProperty("srvarc_user");
				sshPw = authPropertiesFile.getProperty("srvarc_pw");
				sshDestinationDir = authPropertiesFile
						.getProperty("srvarc_syncdir");
			} catch (FileNotFoundException e) {
				LOGGER.severe("Auth Properties not found attempting to read authentication "
						+ "settings. Error: "
						+ e.getClass().getName() + ": " + e.getMessage());
				JOptionPane.showMessageDialog(this,
						"N�o foi poss�vel localizar o arquivo de propriedades "
								+ "de autentica��o no servidor!",
								"Envio de Protocolo", JOptionPane.ERROR_MESSAGE);
				return;
			} catch (IOException e) {
				LOGGER.severe("Output input error attempting to read authentication settings. Error: "
						+ e.getClass().getName() + ": " + e.getMessage());
				JOptionPane
				.showMessageDialog(
						this,
						"N�o foi poss�vel acessar o arquivo de propriedades de autentica��o no servidor!",
						"Envio de Protocolo",
						JOptionPane.ERROR_MESSAGE);
				return;
			} catch (GeneralSecurityException e) {
				LOGGER.severe("Exception at crypt/decrypt algorithm. Error: "
						+ e.getClass().getName() + ": "
						+ e.getMessage());
				JOptionPane
				.showMessageDialog(
						this,
						"Ocorreu um erro ao tentar ler arquivo de autentica��o que est� criptografado. "
								+ "Este erro foi causado por uma falha na descriptografia dos dados!",
								"Envio de Protocolo", JOptionPane.ERROR_MESSAGE);
				return;
			}

			try {
				SystemUtilitiesMethods.doSendEchoRequestToServer(sshHost);
			} catch (UnknownHostException e) {
				LOGGER.severe("Error attempting locate the server. Error: "
						+ e.getClass().getName() + ": " + e.getMessage());
				JOptionPane
				.showMessageDialog(
						this,
						"N�o foi encontrado conex�o com a rede ou o servidor ou computador de "
								+ "destino est� inacess�vel!",
								"Envio de Protocolo", JOptionPane.ERROR_MESSAGE);
				return;
			} catch (IOException e) {
				LOGGER.severe("Error attempting connect to the lan or to the server. Error: "
						+ e.getClass().getName() + ": " + e.getMessage());
				JOptionPane.showMessageDialog(this,
						"Foram encontrados problemas com a rede e n�o foi poss�vel completar a solicita��o!",
						"Envio de Protocolo", JOptionPane.ERROR_MESSAGE);
				return;
			} catch (IllegalArgumentException e) {
				LOGGER.severe("Timeout attempting to locate and connect to the server. Error: "
						+ e.getClass().getName() + ": " + e.getMessage());
				JOptionPane.showMessageDialog(this,
						"Tempo de opera��o excedido ao tentar localizar e conectar ao servidor!",
						"Envio de Protocolo", JOptionPane.ERROR_MESSAGE);
				return;
			}

			DatabasePersistenceDao databasePersistenceDao = new DatabasePersistenceDao();
			String uploadedFile = null;
			try {
				uploadedFile = SystemUtilitiesMethods.doUploadFileToFileServer(
						sshHost, sshUser,
						sshPw, textAttached.getText(), sshDestinationDir,
						textProtocolNumber.getText());
			} catch (Exception e) {
				LOGGER.severe("Attempting to upload file to the server. Error: "
						+ e.getClass().getName() + ": " + e.getMessage());
				JOptionPane.showMessageDialog(this, e.getMessage(),
						"Envio de Protocolo", JOptionPane.ERROR_MESSAGE);
				return;
			}

			String[] params = new String[10];
			params[0] = String.valueOf(sender.getOffice().getId()); // codigo_escritorio
			params[1] = String.valueOf(sender.getId()); // codigo_remetente
			params[2] = sender.getUserName(); // nome_remetente
			params[3] = String.valueOf(((UserReceiver) comboBoxReceiver
					.getSelectedItem()).getId()); // codigo_destinatario
			params[4] = protocolType; // tipo
			params[5] = String.valueOf(((Obligation) comboBoxObligations
					.getSelectedItem()).getId()); // codigo_obrigacao
			params[6] = textProtocolNumber.getText(); // protocolo
			params[7] = new File(uploadedFile).getName(); // anexo
			params[8] = textDescription.getText(); // descricao
			params[9] = registrationDate; // dt_cadastro

			try {
				databasePersistenceDao.saveRecordFromProtocolSent(params);
				sentProtocol = true;
				dispose();
			} catch (ClassNotFoundException e) {
				LOGGER.severe("Driver class connection not found on classpath. Error: "
						+ e.getClass().getName() + ": "
						+ e.getMessage());
				JOptionPane
				.showMessageDialog(
						this,
						"Falha ao conectar ao banco de dados. O driver de conex�o "
								+ "com banco de dados n�o pode ser encontrado!",
								"Envio de Protocolo", JOptionPane.ERROR_MESSAGE);
			} catch (SQLException e) {
				LOGGER.severe("SQL error attempting to save protocol sent to the database table. Error: "
						+ e.getClass().getName() + ": "
						+ e.getMessage());
				JOptionPane.showMessageDialog(
						this,
						"Falha ao realizar opera��o no banco de dados.\nErro: "
								+ e.getMessage(), "Envio de Protocolo",
								JOptionPane.ERROR_MESSAGE);
			} catch (IOException e) {
				LOGGER.severe("Input/output error attempting to save protocol sent to the database table. Error: "
						+ e.getClass().getName() + ": "
						+ e.getMessage());
				JOptionPane.showMessageDialog(
						this,
						"Falha ao tentar ler arquivo de configura��o de conex�o com banco de dados.\nErro: "
								+ e.getMessage(), "Envio de Protocolo",
								JOptionPane.ERROR_MESSAGE);
			} catch (GeneralSecurityException e) {
				LOGGER.severe("Exception at crypt/decrypt algorithm. Error: "
						+ e.getClass().getName() + ": "
						+ e.getMessage());
				JOptionPane
				.showMessageDialog(
						this,
						"Ocorreu um erro ao tentar ler arquivo de autentica��o que est� criptografado. "
								+ "Este erro foi causado por uma falha na descriptografia dos dados!",
								"Envio de Protocolo", JOptionPane.ERROR_MESSAGE);
				return;
			}
		} finally {
			setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			SystemUtilitiesMethods.setDefaultCursor(this);
			btnSend.setEnabled(true);
		}
	}

	/**
	 * M�todo respons�vel por alterar a posi��o horizontalmente (caso n�o seja
	 * um componente JButton) e verticalmente relativamente a posi��o vertical
	 * somando ao tamanho e a constante que armazena a posi��o padr�o relativa
	 * de y (verticalmente).
	 * 
	 * @param componentFrom
	 *            Componente a ser usado para determinar a posi��o horizontal e
	 *            vertical do componente informado no par�metro componentTo.
	 * @param componentTo
	 *            Componente a ser posicionado horizontalmente e verticalmente
	 *            baseado na posi��o do componente informado no par�metro
	 *            componentFrom
	 */
	private void setLocationRelativeToComponent(final Component componentFrom,
			final Component componentTo) {
		int newXLocation;
		if (componentTo instanceof JButton) {
			newXLocation = componentTo.getX();
		} else {
			newXLocation = DEFAULT_POS_X;
		}
		componentTo.setLocation(newXLocation, componentFrom.getY()
				+ componentFrom.getHeight() + DEFAULT_RELATIVE_POS_Y);
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	/**
	 * Evento invocado quando o usu�rio tenta fechar a janela usando o �cone de
	 * fechar janela.
	 */
	@Override
	public void windowClosing(WindowEvent e) {
		if (JOptionPane
				.showConfirmDialog(
						this,
						"O arquivo ainda n�o foi enviado pois seu envio ainda n�o foi confirmado. Deseja cancelar envio?",
						"Cancelamento de Envio de Arquivo",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION) {
			setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		} else {
			setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		}
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}
}
