package br.com.protocolovirtual.jsyncprotocol.gui;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import br.com.protocolovirtual.jsyncprotocol.dao.ConnectionFactory;
import br.com.protocolovirtual.jsyncprotocol.dao.FilePersistenceDao;
import br.com.protocolovirtual.jsyncprotocol.util.SystemUtilitiesConstants;
import br.com.protocolovirtual.jsyncprotocol.util.SystemUtilitiesMethods;

/**
 * Classe que cont�m uma interface gr�fica baseada na classe abstrata
 * {@link AbstractDialogSettingsUI}. Esta classe representa as configura��es de
 * autentica��o com servidor de banco de dados e do servidor de arquivos para
 * onde os protocolos ser�o upados
 */
public class AuthSettingsUI extends AbstractDialogSettingsUI {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4642025948837141294L;
	private JTabbedPane tabPaneServer;
	private JPanel panelTabDatabaseServer;
	private JPanel panelTabFileServer;
	private JLabel lblHostOrIPServData;
	private JLabel lblPortServData;
	private JTextField textPortServData;
	private JTextField textHostOrIPServData;
	private JCheckBox checkBoxDefaultPortServData;
	private JLabel lblDatabaseNameServData;
	private JTextField textDatabaseNameServData;
	private JLabel lblHostOrIPServFiles;
	private JTextField textHostOrIPServFiles;
	private JLabel lblUserServFiles;
	private JLabel lblPwServFiles;
	private JLabel lblSyncPathServFiles;
	private JTextField textUserServFiles;
	private JTextField textSyncPathServFiles;
	private JPasswordField textPwServFiles;

	/**
	 * Create the frame.
	 */
	public AuthSettingsUI() {
		super();
		btnCancel
		.setToolTipText("Clique aqui para cancelar toda a opera\u00E7\u00E3o e encerrar o sistema.");
		btnConfirm
		.setToolTipText("Clique aqui para confirmar e salvar as configura\u00E7\u00F5es e continuar a execu\u00E7\u00E3o do sistema.");
		textPw.setToolTipText("Digite a senha do usu\u00E1rio acima.");
		textUser.setToolTipText("Digite o usu\u00E1rio com permiss\u00E3o para logar no banco de dados.");
		textPw.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		textUser.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		btnConfirm.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					setPropertiesFile();
				}
			}
		});
		btnCancel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					getExitConfirmation();
				}
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getExitConfirmation();
			}
		});
		btnConfirm.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setPropertiesFile();
			}
		});

		lblUser.setLocation(15, 444);
		textUser.setLocation(88, 153);
		lblPw.setLocation(88, 190);
		textPw.setLocation(88, 203);
		btnConfirm.setLocation(170, 240);
		btnCancel.setLocation(269, 240);
		setTitle("Configura\u00E7\u00E3o de Conex\u00E3o com Servidor");
		setName("frameDatabaseSettings");
		setUpComponents();
		addWindowListener(this);
		setPreferredSize(new Dimension(getWidth(), getHeight()));
		pack();
		setDebugDataIntoFields();
	}

	private void setUpComponents() {
		tabPaneServer = new JTabbedPane(JTabbedPane.TOP);
		tabPaneServer.setBounds(88, 41, 270, 225);
		tabPaneServer.setFont(new Font("Verdana", Font.PLAIN, 10));
		tabPaneServer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panelTabDatabaseServer = new JPanel();
		panelTabDatabaseServer.setCursor(Cursor
				.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		panelTabDatabaseServer.setLayout(null);
		panelTabFileServer = new JPanel();
		panelTabFileServer.setCursor(Cursor
				.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		panelTabFileServer.setLayout(null);
		tabPaneServer.add("Servidor de Dados", panelTabDatabaseServer);
		tabPaneServer.add("Servidor de Arquivos", panelTabFileServer);

		lblHostOrIPServFiles = new JLabel("Endere\u00E7o IP/Host");
		lblHostOrIPServFiles.setFont(new Font("Verdana", Font.BOLD, 10));
		lblHostOrIPServFiles.setDisplayedMnemonic(KeyEvent.VK_E);
		lblHostOrIPServFiles.setBounds(0, 5, 114, 14);
		panelTabFileServer.add(lblHostOrIPServFiles);

		textHostOrIPServFiles = new JTextField();
		textHostOrIPServFiles.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		textHostOrIPServFiles
		.setToolTipText("Digite o endere\u00E7o IP ou o nome host do computador para onde os protocolos ser\u00E3o sincronizados.");
		textHostOrIPServFiles.setFont(new Font("Verdana", Font.PLAIN, 10));
		textHostOrIPServFiles.setColumns(10);
		textHostOrIPServFiles.setBounds(0, 20, 270, 26);
		panelTabFileServer.add(textHostOrIPServFiles);
		lblHostOrIPServFiles.setLabelFor(textHostOrIPServFiles);
		lblHostOrIPServFiles.setDisplayedMnemonic(KeyEvent.VK_E);

		lblUserServFiles = new JLabel("Usu\u00E1rio");
		lblUserServFiles.setFont(new Font("Verdana", Font.BOLD, 10));
		lblUserServFiles.setDisplayedMnemonic(KeyEvent.VK_E);
		lblUserServFiles.setBounds(0, 55, 114, 14);
		panelTabFileServer.add(lblUserServFiles);

		lblPwServFiles = new JLabel("Senha");
		lblPwServFiles.setFont(new Font("Verdana", Font.BOLD, 10));
		lblPwServFiles.setDisplayedMnemonic(KeyEvent.VK_E);
		lblPwServFiles.setBounds(0, 105, 114, 14);
		panelTabFileServer.add(lblPwServFiles);

		lblSyncPathServFiles = new JLabel("Diret\u00F3rio a Sincronizar");
		lblSyncPathServFiles.setFont(new Font("Verdana", Font.BOLD, 10));
		lblSyncPathServFiles.setDisplayedMnemonic(KeyEvent.VK_E);
		lblSyncPathServFiles.setBounds(0, 155, 165, 14);
		panelTabFileServer.add(lblSyncPathServFiles);

		textUserServFiles = new JTextField();
		textUserServFiles.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		textUserServFiles
		.setToolTipText("Digite aqui o usu\u00E1rio com permiss\u00E3o de login, leitura e grava\u00E7\u00E3o no servidor remoto de destino.");
		textUserServFiles.setFont(new Font("Verdana", Font.PLAIN, 10));
		textUserServFiles.setColumns(10);
		textUserServFiles.setBounds(0, 70, 270, 26);
		panelTabFileServer.add(textUserServFiles);
		lblUserServFiles.setLabelFor(textUserServFiles);
		lblUserServFiles.setDisplayedMnemonic(KeyEvent.VK_U);

		textSyncPathServFiles = new JTextField();
		textSyncPathServFiles.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		textSyncPathServFiles
		.setToolTipText("Digite aqui o caminho f\u00EDsico do diret\u00F3rio a ser sincronizado no servidor remoto de destino.");
		textSyncPathServFiles.setFont(new Font("Verdana", Font.PLAIN, 10));
		textSyncPathServFiles.setColumns(10);
		textSyncPathServFiles.setBounds(0, 170, 270, 26);
		panelTabFileServer.add(textSyncPathServFiles);
		lblSyncPathServFiles.setLabelFor(textSyncPathServFiles);
		lblSyncPathServFiles.setDisplayedMnemonic(KeyEvent.VK_D);

		textPwServFiles = new JPasswordField();
		textPwServFiles
		.setToolTipText("Digite aqui a senha para o usu\u00E1rio acima.");
		textPwServFiles.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		textPwServFiles.setFont(new Font("Verdana", Font.PLAIN, 10));
		textPwServFiles.setBounds(0, 120, 270, 26);
		panelTabFileServer.add(textPwServFiles);
		lblPwServFiles.setLabelFor(textPwServFiles);
		lblPwServFiles.setDisplayedMnemonic(KeyEvent.VK_S);

		lblHostOrIPServData = new JLabel("Endere\u00E7o IP/Host");
		lblHostOrIPServData.setFont(new Font("Verdana", Font.BOLD, 10));
		lblHostOrIPServData.setBounds(0, 5, 114, 14);
		panelTabDatabaseServer.add(lblHostOrIPServData);

		textHostOrIPServData = new JTextField();
		textHostOrIPServData.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		textHostOrIPServData
		.setToolTipText("Digite o endere\u00E7o IP ou o nome host do computador que se localiza o banco de dados.");
		textHostOrIPServData.setFont(new Font("Verdana", Font.PLAIN, 10));
		textHostOrIPServData.setColumns(10);
		textHostOrIPServData.setBounds(0, 20, 214, 26);
		panelTabDatabaseServer.add(textHostOrIPServData);
		lblHostOrIPServData.setLabelFor(textHostOrIPServData);
		lblHostOrIPServData.setDisplayedMnemonic(KeyEvent.VK_E);

		lblPortServData = new JLabel("Porta");
		lblPortServData.setFont(new Font("Verdana", Font.BOLD, 10));
		lblPortServData.setBounds(222, 6, 40, 14);
		panelTabDatabaseServer.add(lblPortServData);

		textPortServData = new JTextField();
		textPortServData.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		textPortServData
		.setToolTipText("Digite a porta de conex\u00E3o com o banco de dados.");
		textPortServData.setText("3306");
		textPortServData.setFont(new Font("Verdana", Font.PLAIN, 10));
		textPortServData.setEditable(false);
		textPortServData.setColumns(10);
		textPortServData.setBounds(215, 19, 55, 26);
		panelTabDatabaseServer.add(textPortServData);
		lblPortServData.setLabelFor(textPortServData);
		lblPortServData.setDisplayedMnemonic(KeyEvent.VK_P);

		lblDatabaseNameServData = new JLabel("Banco de Dados");
		lblDatabaseNameServData.setFont(new Font("Verdana", Font.BOLD, 10));
		lblDatabaseNameServData.setBounds(0, 55, 114, 14);
		panelTabDatabaseServer.add(lblDatabaseNameServData);

		textDatabaseNameServData = new JTextField();
		textDatabaseNameServData.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		textDatabaseNameServData
		.setToolTipText("Digite o nome do banco de dados.");
		textDatabaseNameServData.setFont(new Font("Verdana", Font.PLAIN, 10));
		textDatabaseNameServData.setColumns(10);
		textDatabaseNameServData.setBounds(0, 70, 270, 26);
		panelTabDatabaseServer.add(textDatabaseNameServData);
		lblDatabaseNameServData.setLabelFor(textDatabaseNameServData);
		lblDatabaseNameServData.setDisplayedMnemonic(KeyEvent.VK_B);

		checkBoxDefaultPortServData = new JCheckBox("Padr\u00E3o");
		checkBoxDefaultPortServData.setSelected(true);
		checkBoxDefaultPortServData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (checkBoxDefaultPortServData.isSelected() == false) {
					textPortServData.setEditable(true);
					textPortServData.setCursor(Cursor
							.getPredefinedCursor(Cursor.TEXT_CURSOR));
				} else {
					textPortServData
					.setText(SystemUtilitiesConstants.SYS_CONNECTION_DEFAULT_PORT);
					textPortServData.setEditable(false);
					textPortServData.setCursor(Cursor
							.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			}
		});
		checkBoxDefaultPortServData
		.setToolTipText("Escolha esta op\u00E7\u00E3o para usar a porta padr\u00E3o. O campo \"Porta\" ficar\u00E1 inalter\u00E1vel.");
		checkBoxDefaultPortServData.setFont(new Font("Verdana", Font.PLAIN, 8));
		checkBoxDefaultPortServData.setBounds(215, 45, 52, 20);
		panelTabDatabaseServer.add(checkBoxDefaultPortServData);
		checkBoxDefaultPortServData.setMnemonic(KeyEvent.VK_A);
		checkBoxDefaultPortServData.setDisplayedMnemonicIndex(1);

		getContentPane().remove(lblUser);
		panelTabDatabaseServer.add(lblUser);
		lblUser.setLocation(0, 105);

		getContentPane().remove(textUser);
		panelTabDatabaseServer.add(textUser);
		textUser.setLocation(0, 120);

		getContentPane().remove(lblPw);
		panelTabDatabaseServer.add(lblPw);
		lblPw.setLocation(0, 155);

		getContentPane().remove(textPw);
		panelTabDatabaseServer.add(textPw);
		textPw.setLocation(0, 170);

		btnConfirm.setLocation(btnConfirm.getX(), btnConfirm.getY() + 30);
		btnCancel.setLocation(btnCancel.getX(), btnCancel.getY() + 30);
		getContentPane().add(tabPaneServer);

		setLocationRelativeToComponent(tabPaneServer, btnConfirm);
		setLocationRelativeToComponent(tabPaneServer, btnCancel);
	}

	/**
	 * M�todo para ser executado em tempo de debug e dever� ser removido quando
	 * for compilado para produ��o.
	 */
	private void setDebugDataIntoFields() {
		textHostOrIPServData.setText("174.142.42.90");
		textDatabaseNameServData.setText("sis_protectinfo");
		textUser.setText("sis_protectinfo");

		textHostOrIPServFiles.setText("protocolovirtual.com.br");
		textUserServFiles.setText("armazenanfe");
		textSyncPathServFiles
		.setText("absprotocolo/protocolo/admin/protocolos/arquivos");
	}

	private boolean checkRequiredFields() {
		String fieldHostOrIPServData = textHostOrIPServData.getText().trim();
		String fieldPortServData = textPortServData.getText().trim();
		String fieldDatabaseNameServData = textDatabaseNameServData.getText()
				.trim();
		String fieldUser = textUser.getText().trim();
		String fieldPw = String.valueOf(textPw.getPassword()).trim();

		if (fieldHostOrIPServData == null || fieldHostOrIPServData.isEmpty()
				|| fieldPortServData == null || fieldPortServData.isEmpty()
				|| fieldDatabaseNameServData == null
				|| fieldDatabaseNameServData.isEmpty() || fieldUser == null
				|| fieldUser.isEmpty() || fieldPw == null || fieldPw.isEmpty()) {
			lblRequiredFields.setVisible(true);
			tabPaneServer.setSelectedIndex(0);
			return false;
		}

		String fieldHostOrIPServFiles = textHostOrIPServFiles.getText().trim();
		String fieldUserServFiles = textUserServFiles.getText().trim();
		String fieldPwServFiles = String.valueOf(textPwServFiles.getPassword())
				.trim();
		String fieldSyncPathServFiles = textSyncPathServFiles.getText().trim();

		if (fieldHostOrIPServFiles == null || fieldHostOrIPServFiles.isEmpty()
				|| fieldUserServFiles == null || fieldUserServFiles.isEmpty()
				|| fieldPwServFiles == null || fieldPwServFiles.isEmpty()
				|| fieldSyncPathServFiles == null
				|| fieldSyncPathServFiles.isEmpty()) {
			lblRequiredFields.setVisible(true);
			tabPaneServer.setSelectedIndex(1);
			return false;
		}

		return true;
	}

	public void setPropertiesFile() {
		// TODO Tratar entrada de caracteres especiais nos campos de texto

		if (checkRequiredFields() == false)
			return;

		String hostServData = textHostOrIPServData.getText().trim();
		String portServData = textPortServData.getText().trim();
		String databaseServData = textDatabaseNameServData.getText().trim();
		String userServData = textUser.getText().trim();
		String pwServData = String.valueOf(textPw.getPassword()).trim();
		String hostServFiles = textHostOrIPServFiles.getText().trim();
		String userServFiles = textUserServFiles.getText().trim();
		String pwServFiles = String.valueOf(textPwServFiles.getPassword())
				.trim();
		String synchronizingDir = textSyncPathServFiles.getText().trim();

		SystemUtilitiesMethods.setWaitProcessCursor(this);

		try {
			ConnectionFactory.doTestConnection(hostServData, databaseServData,
					portServData, userServData, pwServData);
		} catch (ClassNotFoundException | IOException | SQLException e) {
			// TODO Tratar exce��o e Logger
			e.printStackTrace();

			if (e instanceof FileNotFoundException) {

			} else if (e instanceof ClassNotFoundException) {
				JOptionPane
				.showMessageDialog(
						this,
						"A conex�o com o servidor de banco de dados n�o pode ser O driver de conex�o com o banco de dados n�o pode ser carregado "
								+ "novamente.\nCausa do Erro: "
								+ e.getClass().toString()
								+ ": "
								+ e.getMessage(),
								"Erro na conex�o com Servidor de Banco de Dados",
								JOptionPane.ERROR_MESSAGE);
			} else if (e instanceof SQLException) {
				JOptionPane
				.showMessageDialog(
						this,
						"A conex�o com o servidor de banco de dados n�o pode ser realizada. Corrija e tente "
								+ "novamente.\nCausa do Erro: "
								+ e.getClass().toString()
								+ ": "
								+ e.getMessage(),
								"Erro na conex�o com Servidor de Banco de Dados",
								JOptionPane.ERROR_MESSAGE);
			}

			tabPaneServer.setSelectedIndex(0);
			SystemUtilitiesMethods.setDefaultCursor(this);
			return;
		}

		try {
			SystemUtilitiesMethods.doUploadFileToFileServer(hostServFiles,
					userServFiles, pwServFiles, null, synchronizingDir, null);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Houve falha ao tentar conectar no servidor de arquivos.\nCausa do Erro: "
							+ e.getClass().toString() + ": " + e.getMessage(),
							"Erro na conex�o com Servidor de Arquivos",
							JOptionPane.ERROR_MESSAGE);

			tabPaneServer.setSelectedIndex(1);
			SystemUtilitiesMethods.setDefaultCursor(this);
			return;
		}

		Map<String, String> propertiesKeyValues = new LinkedHashMap<>(9);

		propertiesKeyValues.put("srvdb_host", hostServData);
		propertiesKeyValues.put("srvarc_host", hostServFiles);
		propertiesKeyValues.put("srvarc_syncdir", synchronizingDir);
		propertiesKeyValues.put("srvdb_port", portServData);
		propertiesKeyValues.put("srvdb_user", userServData);
		propertiesKeyValues.put("srvarc_pw", pwServFiles);
		propertiesKeyValues.put("srvdb_pw", pwServData);
		propertiesKeyValues.put("srvarc_user", userServFiles);
		propertiesKeyValues.put("srvdb_dbname", databaseServData);

		FilePersistenceDao filePersistenceDao = new FilePersistenceDao();
		try {
			filePersistenceDao.setAuthPropertiesFile(propertiesKeyValues);
		} catch (GeneralSecurityException | IOException e) {
			// TODO Tratar exce��o e Logger

			if (e instanceof FileNotFoundException) {

			} else if (e instanceof IOException) {

			} else {

			}

			e.printStackTrace();

			SystemUtilitiesMethods.setDefaultCursor(this);
			return;
		}

		SystemUtilitiesMethods.setDefaultCursor(this);
		JOptionPane.showMessageDialog(this,
				"Configura��o do servidor realizada com sucesso!",
				"Configura��o de Conex�o", JOptionPane.INFORMATION_MESSAGE);
		dispose();
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		if (JOptionPane
				.showConfirmDialog(
						this,
						"As configura��es de conex�o n�o foram confirmadas. Deseja cancelar configura��o?",
						"Cancelamento das Configura��es de Conex�o",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION) {
			setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		} else {
			System.exit(0);
		}
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
		setAlwaysOnTop(true);
	}
}
