package br.com.protocolovirtual.jsyncprotocol.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * Classe abstrata utilizada como modelo de layout para {@link AuthSettingsUI} e
 * {@link SystemSettingsUI}
 */
public abstract class AbstractDialogSettingsUI extends JDialog implements
WindowListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6729642891901814073L;
	protected static final int DEFAULT_POS_X = 88;
	protected static final int DEFAULT_RELATIVE_POS_Y = 10;
	protected JLabel lblUser;
	protected JLabel lblPw;
	protected JTextField textUser;
	protected JPasswordField textPw;
	protected JButton btnCancel;
	protected JButton btnConfirm;
	protected JLabel lblRequiredFields;

	/**
	 * Create the frame.
	 */
	public AbstractDialogSettingsUI() {
		setName("frameAbstractFrameModel");
		getContentPane().setFont(new Font("Verdana", Font.PLAIN, 10));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 375);
		setPreferredSize(new Dimension(getWidth(), getHeight()));
		setLocationRelativeTo(this);
		getContentPane().setLayout(null);
		setResizable(false);
		setModal(true);
		setUpComponents();
		pack();
	}

	/**
	 * M�todo respons�vel por exibir uma mensagem de confirma��o para o usu�rio,
	 * ao ser solicitado o cancelamento de alguma configura��o
	 */
	public void getExitConfirmation() {
		if (JOptionPane.showConfirmDialog(this,
				"Confirma cancelamento? O sistema ser� encerrado.",
				"Confirma��o", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}

	/**
	 * M�todo respons�vel por exibir a label que exibe mensagem dos campos
	 * requeridos
	 */
	public void setRequiredFieldsLabelUnvisible() {
		if (lblRequiredFields.isVisible()) {
			lblRequiredFields.setVisible(false);
		}
	}

	private void setUpComponents() {
		lblUser = new JLabel("Usu\u00E1rio");
		lblUser.setFont(new Font("Verdana", Font.BOLD, 10));
		lblUser.setBounds(88, 91, 114, 14);
		getContentPane().add(lblUser);

		textUser = new JTextField();
		textUser.setFont(new Font("Verdana", Font.PLAIN, 10));
		textUser.setBounds(88, 104, 270, 26);
		getContentPane().add(textUser);
		textUser.setColumns(10);
		lblUser.setLabelFor(textUser);
		lblUser.setDisplayedMnemonic(KeyEvent.VK_U);

		lblPw = new JLabel("Senha");
		lblPw.setFont(new Font("Verdana", Font.BOLD, 10));
		lblPw.setBounds(88, 141, 114, 14);
		getContentPane().add(lblPw);

		textPw = new JPasswordField();
		textPw.setFont(new Font("Verdana", Font.PLAIN, 10));
		textPw.setBounds(88, 154, 270, 26);
		getContentPane().add(textPw);
		lblPw.setLabelFor(textPw);
		lblPw.setDisplayedMnemonic(KeyEvent.VK_S);

		btnConfirm = new JButton("Confirmar");
		btnConfirm.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnConfirm.setBounds(170, 191, 89, 26);
		btnConfirm.setMnemonic(KeyEvent.VK_C);
		btnConfirm.setDisplayedMnemonicIndex(0);
		getContentPane().add(btnConfirm);

		btnCancel = new JButton("Cancelar");
		btnCancel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnCancel.setBounds(269, 191, 89, 26);
		btnCancel.setMnemonic(KeyEvent.VK_N);
		btnCancel.setDisplayedMnemonicIndex(2);
		getContentPane().add(btnCancel);

		lblRequiredFields = new JLabel("Todos os campos s\u00E3o requeridos!");
		lblRequiredFields.setVisible(false);
		lblRequiredFields.setFont(new Font("Verdana", Font.BOLD, 12));
		lblRequiredFields.setForeground(Color.RED);
		lblRequiredFields.setBounds(88, 20, 270, 14);
		getContentPane().add(lblRequiredFields);
	}

	/**
	 * M�todo respons�vel por alterar a posi��o horizontalmente (caso n�o seja
	 * um componente JButton) e verticalmente relativamente a posi��o vertical
	 * somando ao tamanho e a constante que armazena a posi��o padr�o relativa
	 * de y (verticalmente).
	 * 
	 * @param componentFrom
	 *            Componente a ser usado para determinar a posi��o horizontal e
	 *            vertical do componente informado no par�metro componentTo.
	 * @param componentTo
	 *            Componente a ser posicionado horizontalmente e verticalmente
	 *            baseado na posi��o do componente informado no par�metro
	 *            componentFrom
	 */
	public void setLocationRelativeToComponent(final Component componentFrom,
			final Component componentTo) {
		int newXLocation;
		if (componentTo instanceof JButton) {
			newXLocation = componentTo.getX();
		} else {
			newXLocation = DEFAULT_POS_X;
		}
		componentTo.setLocation(newXLocation, componentFrom.getY()
				+ componentFrom.getHeight() + DEFAULT_RELATIVE_POS_Y);
	}
}
