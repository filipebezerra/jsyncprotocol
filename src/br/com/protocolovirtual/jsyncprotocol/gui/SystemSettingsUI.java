package br.com.protocolovirtual.jsyncprotocol.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import br.com.protocolovirtual.jsyncprotocol.dao.DatabasePersistenceDao;
import br.com.protocolovirtual.jsyncprotocol.dao.FilePersistenceDao;
import br.com.protocolovirtual.jsyncprotocol.model.UserSender;
import br.com.protocolovirtual.jsyncprotocol.service.WatchDirService;
import br.com.protocolovirtual.jsyncprotocol.util.SystemUtilitiesConstants;
import br.com.protocolovirtual.jsyncprotocol.util.SystemUtilitiesMethods;

/**
 * Classe que cont�m uma interface gr�fica baseada na classe abstrata
 * {@link AbstractDialogSettingsUI}. Esta classe representa as configura��es do
 * sistema como o usu�rio logado e a pasta � ser monitorada pelo servi�o
 * {@link WatchDirService}
 */
public class SystemSettingsUI extends AbstractDialogSettingsUI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1156182842843614790L;

	/**
	 * Create the frame.
	 */
	public SystemSettingsUI() {
		super();
		setPreferredSize(new Dimension(450, 225));
		textUser.setBackground(Color.WHITE);
		textPw.setBackground(Color.WHITE);
		textPw.setToolTipText("Digite a senha do usu\u00E1rio acima.");
		lblUser.setText("Usu\u00E1rio/Email");
		textPw.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		textUser.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				setRequiredFieldsLabelUnvisible();
			}
		});
		btnConfirm.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(java.awt.event.KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					setPropertiesFile();
				}
			}
		});
		btnConfirm.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setPropertiesFile();
			}
		});
		btnCancel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					getExitConfirmation();
				}
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getExitConfirmation();
			}
		});
		textUser.setToolTipText("Digite o usu\u00E1rio o qual deseja logar e ser o remetente dos protocolos.");
		btnCancel
		.setToolTipText("Clique aqui para cancelar toda a opera\u00E7\u00E3o e encerrar o sistema.");
		btnConfirm
		.setToolTipText("Clique aqui para confirmar e salvar as configura\u00E7\u00F5es e continuar "
				+ "a execu\u00E7\u00E3o do sistema.");
		setTitle("Autentica\u00E7\u00E3o de Usu\u00E1rio");
		setName("frameAuthUser");
		setUpComponents();
		addWindowListener(this);
		pack();
	}

	@Override
	public void getExitConfirmation() {
		if (JOptionPane
				.showConfirmDialog(
						this,
						"As configura��es do sistema n�o foram confirmadas. Deseja cancelar configura��o?",
						"Cancelamento das Configura��es do Sistema",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION) {
			setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		} else {
			FilePersistenceDao filePersistenceDao = new FilePersistenceDao();
			try {
				if (filePersistenceDao.isConfiguredSystemPropertiesFile() == false) {
					System.exit(0);
				} else {
					dispose();
				}
			} catch (FileNotFoundException ex) {
				JOptionPane
				.showMessageDialog(
						null,
						"N�o foi poss�vel localizar o arquivo de propriedades para configurar a autentica��o no servidor!",
						"Configura��o do Sistema",
						JOptionPane.ERROR_MESSAGE);
			} catch (IOException ex) {
				JOptionPane
				.showMessageDialog(
						null,
						"N�o foi poss�vel acessar o arquivo de propriedades para configurar a autentica��o no servidor!",
						"Configura��o do Sistema",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void setUpComponents() {
		lblUser.setBounds(88, 41, 114, 14);
		textUser.setBounds(88, 56, 270, 26);
		lblPw.setBounds(88, 91, 114, 14);
		textPw.setBounds(88, 106, 270, 26);
		btnConfirm.setLocation(170, 144);
		btnCancel.setLocation(269, 144);

		setLocationRelativeToComponent(textPw, btnConfirm);
		setLocationRelativeToComponent(textPw, btnCancel);
	}

	private boolean checkRequiredFields() {
		String userName = textUser.getText().trim();
		String pw = String.valueOf(textPw.getPassword());

		if (userName == null || userName.isEmpty() || pw == null
				|| pw.isEmpty()) {
			lblRequiredFields.setVisible(true);
			return false;
		}

		return true;
	}

	public void setPropertiesFile() {
		if (checkRequiredFields() == false)
			return;

		String userName = textUser.getText().trim();
		String pw = String.valueOf(textPw.getPassword());

		SystemUtilitiesMethods.setWaitProcessCursor(this);

		UserSender sender = null;
		DatabasePersistenceDao databasePersistenceDao = new DatabasePersistenceDao();
		try {
			sender = databasePersistenceDao.retrieveUserSender(userName, pw);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			JOptionPane
			.showMessageDialog(
					this,
					"Ocorreu uma falha ao tentar comunicar com servidor de banco de dados.\nCausa do erro: "
							+ e.getClass().toString()
							+ ": "
							+ e.getMessage(),
							"Erro ao comunicar com servidor",
							JOptionPane.ERROR_MESSAGE);
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (sender != null) {
			Map<String, String> propertiesKeyValues = new LinkedHashMap<>(9);

			propertiesKeyValues.put("logged_officeid", String.valueOf(sender.getOffice().getId()));
			propertiesKeyValues.put("logged_officename", sender.getOffice().getName());
			propertiesKeyValues.put("logged_userid", String.valueOf(sender.getId()));
			propertiesKeyValues.put("logged_realname", sender.getName());
			propertiesKeyValues.put("logged_username", sender.getUserName());
			propertiesKeyValues.put("watched_folder", SystemUtilitiesConstants.PATH_PROTOCOLS);

			FilePersistenceDao filePersistenceDao = new FilePersistenceDao();
			try {
				filePersistenceDao.setSystemPropertiesFile(propertiesKeyValues);

				JOptionPane.showMessageDialog(this,
						"Usu�rio configurado com sucesso!",
						"Configura��o de Usu�rio",
						JOptionPane.INFORMATION_MESSAGE);
				setVisible(false);
			} catch (FileNotFoundException e) {
				JOptionPane
				.showMessageDialog(
						null,
						"N�o foi poss�vel localizar o arquivo de propriedades para ler as configura��es o sistema!",
						"Configura��o do Sistema",
						JOptionPane.ERROR_MESSAGE);
			} catch (IOException e) {
				JOptionPane
				.showMessageDialog(
						null,
						"N�o foi poss�vel acessar o arquivo de propriedades para ler as configura��es do sistema!",
						"Configura��o do Sistema",
						JOptionPane.ERROR_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(this,
					"O usu�rio ou senha informado est�o incorretos. "
							+ "Tente novamente!",
							"Erro ao tentar realizar login",
							JOptionPane.ERROR_MESSAGE);
		}

		SystemUtilitiesMethods.setDefaultCursor(this);
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		getExitConfirmation();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
		setAlwaysOnTop(true);
	}
}
