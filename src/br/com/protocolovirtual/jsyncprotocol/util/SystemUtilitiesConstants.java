package br.com.protocolovirtual.jsyncprotocol.util;

import java.awt.Cursor;

/**
 * Classe repons�vel por fornece todas contantes utilit�rias necess�rias �
 * outras classes da aplica��o
 * 
 */
public class SystemUtilitiesConstants {
	/**
	 * Constante do caminho do arquivo de texto para teste de conex�o e upload
	 * com servidor remoto de destino usando o protocolo SSH.
	 */
	public static final String RES_FILE_TEST_UPLOAD_SSH = "./res/txt/textUploadSSH.txt";

	/**
	 * Constante do caminho do arquivo de propriedades que cont�m as
	 * configura��es do sistema.
	 */
	public static final String RES_PROPERTIES_SYSTEM_PATH = "./res/prop/system-conf.properties";

	/**
	 * Constante do caminho do arquivo de propriedades que cont�m as vari�veis
	 * de autentica��o com servidor destino.
	 */
	public static final String RES_PROPERTIES_AUTH_PATH = "./res/prop/auth-conf.properties";

	/**
	 * Constante do caminho do arquivo de propriedades que cont�m as constantes
	 * de conex�o usadas pelo driver jdbc.
	 */
	public static final String RES_PROPERTIES_JDBC_PATH = "./res/prop/jdbc-conf.properties";

	/**
	 * Constante do caminho do arquivo de recurso do sistema, a imagem logo do
	 * �cone que estar� alojado na barra de notifica��o
	 */
	public static final String RES_IMAGE_TRAY_ICON = "res/img/JSynchProtocol.gif";

	/**
	 * Constante do nome da diret�rio onde o sistema ir� monitorar por novos
	 * arquivos categorizados como protocolo.
	 */
	public static final String PATH_PROTOCOLS = "C:\\Protocolo";

	/**
	 * Constante do nome da diret�rio para onde os arquivos enviados ser�o
	 * movidos
	 */
	public static final String DIR_NAME_PROTOCOLS_SENT = "Documentos enviados";

	/**
	 * Constante do nome da diret�rio para onde os arquivos n�o enviados ser�o
	 * movidos
	 */
	public static final String DIR_NAME_PROTOCOLS_NOT_SENT = "Documentos n�o enviados";

	/**
	 * Constante refer�ncia para o cursor padr�o do sistema operacional
	 */
	public static final Cursor SYS_WINDOW_CURSOR_DEFAULT = Cursor
			.getDefaultCursor();

	/**
	 * Constante refer�ncia para o cursor de tarefa em processamento do sistema
	 * operacional
	 */
	public static final Cursor SYS_WINDOW_CURSOR_WAIT_PROCESS = Cursor
			.getPredefinedCursor(Cursor.WAIT_CURSOR);

	/**
	 * Constante da porta padr�o de conex�o com banco de dados. Espec�fico para
	 * o MySQL.
	 */
	public static final String SYS_CONNECTION_DEFAULT_PORT = "3306";
}
