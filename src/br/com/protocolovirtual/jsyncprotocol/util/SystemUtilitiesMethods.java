package br.com.protocolovirtual.jsyncprotocol.util;

import java.awt.AWTException;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.DirectoryNotEmptyException;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

import br.com.protocolovirtual.jsyncprotocol.gui.SystemSettingsUI;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.SCPClient;
import ch.ethz.ssh2.Session;

/**
 * Classe respons�vel por m�todos gen�ricos e utilit�rios � aplica��o e sua que
 * s�o utilizados por v�rias classes. Esta classe � composta por m�todos
 * est�ticos.
 */
public class SystemUtilitiesMethods {
	/**
	 * M�todo respons�vel por formatar uma sequ�ncia de caracteres para um
	 * tamanho especificado utilizando o caracter informado incluindo-o �
	 * esquerda ou � direta do texto.
	 * 
	 * @param text
	 *            o texto � ser formatado usando o padr�o passado nos
	 *            par�metros.
	 * @param size
	 *            o tamanho final que o texto deve conter.
	 * @param character
	 *            o caracter que ser� usado para ser acrescido no texto
	 *            especificado.
	 * @param toLeft
	 *            caso seja informado um valor <b>true</b> o caracter ser�
	 *            acrescido � esquerda do texto, sen�o � direita.
	 * @return o texto formado de acordo com os padr�es passados nos argumentos.
	 */
	public static String formatCharAmountOf(final String text, final int size,
			final char character, final boolean toLeft) {
		StringBuffer buffer = new StringBuffer(text);
		for (int i = 1; i <= size; i++) {
			if (buffer.length() == size) {
				break;
			}
			if (toLeft) {
				buffer.insert(0, character);
			} else {
				buffer.append(character);
			}
		}
		return buffer.toString();
	}

	/**
	 * M�todo respons�vel por modificar o cursor da janela para o cursor padr�o.
	 * 
	 * @param window
	 *            a janela a qual modificar� o cursor. Pode ser JDialog ou
	 *            JFrame.
	 */
	public static void setDefaultCursor(Window window) {
		if (!(window instanceof JDialog) && !(window instanceof JFrame))
			return;
		Cursor cursor = SystemUtilitiesConstants.SYS_WINDOW_CURSOR_DEFAULT;
		Container windowContainer = window instanceof JDialog ? ((JDialog) window)
				.getContentPane() : ((JFrame) window).getContentPane();
				windowContainer.setCursor(cursor);
	}

	/**
	 * M�todo respons�vel por modificar o cursor da janela para o cursor de
	 * processamento.
	 * 
	 * @param window
	 *            a janela a qual modificar� o cursor. Pode ser JDialog ou
	 *            JFrame.
	 */
	public static void setWaitProcessCursor(Window window) {
		if (!(window instanceof JDialog) && !(window instanceof JFrame))
			return;
		Cursor cursor = SystemUtilitiesConstants.SYS_WINDOW_CURSOR_WAIT_PROCESS;
		Container windowContainer = window instanceof JDialog ? ((JDialog) window)
				.getContentPane() : ((JFrame) window).getContentPane();
				windowContainer.setCursor(cursor);
	}

	/**
	 * M�todo respons�vel por criar as pastas de destino dos protocolos
	 * sincronizados ou n�o.
	 * 
	 * @throws DirectoryNotEmptyException
	 *             Erro por n�o ter sido criado corretamente os diret�rios.
	 * @throws IOException
	 *             Erro por n�o localizar diret�rio ou n�o ter acesso de
	 *             leitura/escrita no diret�rio de destino.
	 */
	public static String doMakeDir()
			throws SecurityException, DirectoryNotEmptyException {
		String[] paths = new String[3];
		File[] directories = new File[3];

		paths[0] = SystemUtilitiesConstants.PATH_PROTOCOLS;
		paths[1] = paths[0] + File.separator
				+ SystemUtilitiesConstants.DIR_NAME_PROTOCOLS_SENT;
		paths[2] = paths[0] + File.separator
				+ SystemUtilitiesConstants.DIR_NAME_PROTOCOLS_NOT_SENT;

		for (int i = 0; i < directories.length; i++) {
			directories[i] = new File(paths[i]);
			if (directories[i].exists() == false) {
				directories[i].mkdir();
			}
		}

		for (File directorie : directories) {
			if (directorie.exists() == false)
				throw new DirectoryNotEmptyException("O diret�rio "
						+ directorie.getName()
						+ " n�o p�de ser criado corretamente!");
		}

		return paths[0];
	}

	/**
	 * M�todo respons�vel por criar e posicionar o �cone da aplica��es e seus
	 * menus na barra de notifica��o do windows.
	 */
	public static void doShowTrayIcon() {
		/**
		 * Verifica se o sistema operacional suporta tray icon
		 */
		if (SystemTray.isSupported()) {

			/**
			 * Obtem a bandeja do sistema operacioanl
			 */
			SystemTray tray = SystemTray.getSystemTray();

			Image image = Toolkit.getDefaultToolkit().getImage(
					SystemUtilitiesConstants.RES_IMAGE_TRAY_ICON);

			final PopupMenu popup = new PopupMenu();

			MenuItem menuItemChangeLoggedUser = new MenuItem(
					"Trocar usu�rio logado");
			menuItemChangeLoggedUser.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					SystemSettingsUI systemSettingsWindow = new SystemSettingsUI();
					systemSettingsWindow.setVisible(true);
					systemSettingsWindow.dispose();
				}
			});
			popup.add(menuItemChangeLoggedUser);

			popup.addSeparator();

			MenuItem menuItemStopWatchingService = new MenuItem("Parar Sistema");
			menuItemStopWatchingService.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (JOptionPane
							.showConfirmDialog(
									null,
									"Parar o monitoramento e sincroniza��o dos protocolos?",
									"Confirma��o", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
						System.exit(0);
					}
				}
			});
			popup.add(menuItemStopWatchingService);

			TrayIcon trayIcon = new TrayIcon(image, "Protocolo Virtual", popup);

			trayIcon.setImageAutoSize(true);

			trayIcon.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					popup.show(null, 0, 0);
				}
			});

			try {
				tray.add(trayIcon);
				trayIcon.displayMessage("Informa��o",
						"Sistema em execu��o e monitorando!",
						TrayIcon.MessageType.INFO);
			} catch (AWTException e) {
				System.err
				.println("TrayIcon n�o pode ser adicionado no sistema.");
			}
		} else {
			System.err.println("Bandeja do sistema n�o � suportado.");
		}
	}

	/**
	 * M�todo respons�vel por definir a apar�ncia das janelas aplica��o para o
	 * estilo LookAndFeel Nimbus.
	 * 
	 * @throws UnsupportedLookAndFeelException
	 *             Erro por n�o ser um look and feel suportado ou n�o foi
	 *             localizado no sistema.
	 */
	public static void setNimbusLookAndFeel()
			throws UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(new NimbusLookAndFeel());
	}

	/**
	 * M�todo respons�vel por testar a conectividade com o host informado. Um
	 * pacote � enviado ao host e seu endere�o IP � retornado juntamente com um
	 * teste se o host informado � alcan��vel.
	 * 
	 * @param hostAddress
	 *            o endere�o IP ou nome do host � ser testado.
	 * @return a veracidade se o host informado pode ser encontrado e se pode
	 *         ser alcan�ado pelo pacote ICMP enviado.
	 * @throws IOException
	 *             Erro se problemas com a rede ocorrerem
	 * @throws UnknownHostException
	 *             Erro se o host informado n�o pode ser encontrado.
	 * @throws IllegalArgumentException
	 *             Erro se o tempo limite for negativo. O tempo padr�o utilizado
	 *             � de 5 segundos.
	 */
	public static boolean doSendEchoRequestToServer(final String hostAddress)
			throws IOException, UnknownHostException, IllegalArgumentException {
		InetAddress inetAddress = InetAddress.getByName(hostAddress);
		return inetAddress.getHostAddress() != null
				&& inetAddress.isReachable(5000);
	}

	/**
	 * M�todo respons�vel por executar o upload do arquivo (comumente um
	 * protocolo) no sistema de arquivos do servidor destino informado nos
	 * argumentos.
	 * 
	 * @param host
	 *            o servidor ou m�quina de destino.
	 * @param user
	 *            o usu�rio para logar, com permiss�es de leitura e escrita.
	 * @param pw
	 *            a senha do usu�rio a logar.
	 * @param from
	 *            o endere�o f�sico do arquivo a ser sincronizado.
	 * @param to
	 *            o diret�rio de destino no sistema de arquivos do servidor ou
	 *            m�quina de destino.
	 * @param renameFrom
	 * @return
	 * @throws Exception
	 * @throws IOException
	 */
	public static String doUploadFileToFileServer(final String host,
			final String user, final String pw, final String from,
			final String to, final String renameFrom) throws Exception,
			IOException {
		Connection connection = new Connection(host);
		try {
			connection.connect();
		} catch (IOException e) {
			throw new IOException(
					"O m�todo de autentica��o por senha n�o foi aceito pelo servidor!");
		}

		if (connection.authenticateWithPassword(user, pw)) {
			SCPClient client = new SCPClient(connection);
			try {
				if (from == null) {
					client.put(
							SystemUtilitiesConstants.RES_FILE_TEST_UPLOAD_SSH,
							to);
					return new File(
							SystemUtilitiesConstants.RES_FILE_TEST_UPLOAD_SSH)
					.getName();
				} else {
					client.put(from, to);
					String fileSynchronized = new File(from).getName();
					Session session = null;
					try {
						session = connection.openSession();

						File source = new File(to + "/"
								+ new File(from).getName());
						String newFileName = FileUtilities.changeFileName(
								source.getPath(), renameFrom).replace("\\", "/");

						String oldFileName = source.getPath().replace("\\", "/").replace(" ", "\\ ");
						session.execCommand("mv " + oldFileName + " " + newFileName);
						fileSynchronized = new File(newFileName).getName();
					} catch (IOException e) {
						throw new IOException(
								"N�o foi poss�vel renomear o arquivo "
										+ from
										+ " que foi sincronizado com o mesmo nome ao inv�s de "
										+ renameFrom
										+ ". Favor conecte ao servidor "
										+ host
										+ " e renomeie este arquivo que est� armazenado na pasta "
										+ to + ".");
					} finally {
						if (session != null) {
							session.close();
						}
					}

					return fileSynchronized;
				}
			} catch (IOException e) {
				throw new IOException(
						"Falha durante a transfer�ncia do arquivo. O arquivo "
								+ from
								+ " est� corrompido ou n�o foi localizado, ou o diret�rio "
								+ to
								+ " parece n�o existir ou o usu�rio n�o tem permiss�es de escrita!");
			} finally {
				connection.close();
			}
		} else
			throw new IOException(
					"Falha na autentica��o. Usu�rio ou senha incorreto!");
	}

	public static void doConnectToDatabaseServer() {

	}
}
