package br.com.protocolovirtual.jsyncprotocol.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import br.com.protocolovirtual.jsyncprotocol.service.WatchDirService;

/**
 * Classe respons�vel por realizar opera��es em arquivos e diret�rios. Esta
 * classe � composta de m�todos est�ticos e constantes que realizam opera��es no
 * sistema de arquivos, operando sobre arquivos e diret�rios.
 */
public class FileUtilities {
	/**
	 * Constante que armazena o separador de diret�rios padr�o do sistema de
	 * arquivos. No windows o separador padr�o � o '\\' e no unix � '/'.
	 */
	public static final String SEPARATOR = File.separator;

	/**
	 * M�todo respons�vel por renomear conceitualmente, o arquivo e n�o
	 * fisicamente.
	 * 
	 * @param fileName
	 *            o caminho do arquivo a ser renomeado.
	 * @param newName
	 *            o nome a substituir o nome atual do arquivo passado como
	 *            argumento.
	 * @return o novo nome do arquivo.
	 * @throws IOException
	 *             Erro ao tentar acessar, ler ou escrever o arquivo em disco.
	 */
	public static String changeFileName(final String fileName, final String newName)
			throws IOException {
		Path source = Paths.get(fileName);
		String sourceExt = getFileExtension(source.toFile());
		Path target = Paths.get(source.getParent().toString() + SEPARATOR
				+ newName + sourceExt);

		return target.toString();
	}

	/**
	 * M�todo respons�vel por extrair a extens�o de um determinado arquivo.
	 * 
	 * @param file
	 *            o arquivo a extrair extens�o. Neste m�todo n�o possibilidade
	 *            de um diret�rio ser passado como argumento. No m�todo
	 *            {@link WatchDirService#processEvents()} � verificado se o
	 *            evento gerado � de origem de um diret�rio, caso seja a
	 *            opera��o � avisada ao usu�rio e ignorado pelo sistema.
	 * @return a extens�o do arquivo passado como argumento. � incluso no
	 *         retorno o ponto (.) que antecede a extens�o do arquivo.
	 */
	public static String getFileExtension(final File file) {
		String[] tokens = file.getName().split("\\.");
		return "." + tokens[tokens.length - 1];
	}

	/**
	 * M�todo respons�vel por renomear e mover o arquivo de protocolo para os
	 * diret�rios padr�es. Caso o protocolo foi enviado com sucesso o arquivo �
	 * movido para {@link SystemUtilitiesConstants#DIR_NAME_PROTOCOLS_SENT},
	 * caso o usu�rio escolheu n�o enviar o arquivo � movido para
	 * {@link SystemUtilitiesConstants#DIR_NAME_PROTOCOLS_NOT_SENT} e se
	 * porventura ocorreu algum erro no envio do arquivo � movido para a �rea de
	 * trabalho do usu�rio logado no windows.
	 * 
	 * @param fileSource
	 *            o arquivo de origem, ou seja, o protocolo enviado. Caso seja
	 *            passado como nulo (<code>null</code>), � pressuposto que o
	 *            protocolo n�o foi enviado por decis�o do usu�rio.
	 * @param protocolNumber
	 *            o n�mero do protocolo gerado, em caso do protocolo ter sido
	 *            enviado.
	 * @throws IOException
	 *             Erro ao tentar acessar, ler ou escrever o arquivo em disco.
	 */
	public static void moveFile(final File fileSource,
			final String protocolNumber) throws IOException {
		boolean isSentFile = protocolNumber != null;
		Path source = fileSource.toPath();
		String sourceExt = getFileExtension(source.toFile());
		Path target = Paths
				.get(source.getParent().toString()
						+ SEPARATOR
						+ (isSentFile ? SystemUtilitiesConstants.DIR_NAME_PROTOCOLS_SENT
								: SystemUtilitiesConstants.DIR_NAME_PROTOCOLS_NOT_SENT)
								+ SEPARATOR
								+ (isSentFile ? protocolNumber : source.toFile()
										.getName().replace(sourceExt, "")
										+ "_"
										+ DateTimeUtilities
										.getDateNowAsString("ddMMyyyy_HHmmss"))
										+ sourceExt);

		Files.move(source, target, StandardCopyOption.ATOMIC_MOVE);
	}
}
