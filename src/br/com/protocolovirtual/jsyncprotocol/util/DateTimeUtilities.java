package br.com.protocolovirtual.jsyncprotocol.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 */
public class DateTimeUtilities {
	/**
	 * M�todo respons�vel por formatar e retornar uma data usando o padr�o de
	 * formato <b>dd/MM/yyyy HH:mm:ss</b>
	 * 
	 * @param data
	 *            a data a ser formata e retornada como texto
	 * @return um texto com uma data formatada no padr�o citado acima
	 */
	public static String getDateAsString(final Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"dd/MM/yyyy HH:mm:ss");
		return dateFormat.format(date);
	}

	/**
	 * M�todo respons�vel por formatar e retornar a data corrente do sistema
	 * usando o padr�o de formato <b>dd/MM/yyyy HH:mm:ss</b>
	 * 
	 * @return um texto com uma data formatada no padr�o citado acima
	 */
	public static String getDateNowAsString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"dd/MM/yyyy HH:mm:ss");
		return dateFormat.format(new Date());
	}

	/**
	 * M�todo respons�vel por formatar e retornar a data corrente do sistema
	 * usando o padr�o de formato passado como argumento
	 * 
	 * @param pattern
	 *            o padr�o para o formato da data a ser formatada
	 * @return um texto com uma data formatada no padr�o citado acima
	 */
	public static String getDateNowAsString(final String pattern) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(new Date());
	}
}
