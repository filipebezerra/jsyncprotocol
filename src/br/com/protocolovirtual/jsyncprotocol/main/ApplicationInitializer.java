package br.com.protocolovirtual.jsyncprotocol.main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.UnsupportedLookAndFeelException;

import br.com.protocolovirtual.jsyncprotocol.dao.FilePersistenceDao;
import br.com.protocolovirtual.jsyncprotocol.gui.AuthSettingsUI;
import br.com.protocolovirtual.jsyncprotocol.gui.SystemSettingsUI;
import br.com.protocolovirtual.jsyncprotocol.logging.HandleLogger;
import br.com.protocolovirtual.jsyncprotocol.service.WatchDirService;
import br.com.protocolovirtual.jsyncprotocol.util.SystemUtilitiesMethods;

/**
 * Classe respons�vel por inicilizar a aplica��o e chamar pelas classes gr�ficas
 * de configura��o e pela classe {@link WatchDirService}
 * 
 */
public class ApplicationInitializer {
	/**
	 * Membro respons�vel por registrar as mensagens desta classe. Para cada
	 * objeto logger existir� um determinado n�vel para registrar as mensagens,
	 * em quase todos os objetos este n�vel ser� o
	 * java.util.logging.Level.CONFIG
	 */
	private static final Logger LOGGER = Logger.getLogger(WatchDirService.class
			.getCanonicalName());

	/**
	 * @param args
	 *            Argumentos de entrada da aplica��o
	 */
	public static void main(String[] args) {
		LOGGER.setLevel(Level.CONFIG);

		try {
			HandleLogger.setup();
		} catch (IOException e) {
			LOGGER.severe("Attempting to setup the logger handlers. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					null,
					"Problemas com a cria��o dos arquivos de log do sistema!",
					"Configura��o de log do sistema",
					JOptionPane.ERROR_MESSAGE);
		}

		try {
			SystemUtilitiesMethods.setNimbusLookAndFeel();
		} catch (UnsupportedLookAndFeelException e) {
			LOGGER.severe("Attempting to setup the look and feel. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					null,
					"N�o foi poss�vel aplicar a apar�ncia defina por padr�o para o sistema!",
					"Configura��o de LookAndFeel do sistema",
					JOptionPane.ERROR_MESSAGE);
		}

		String watchindDir = null;
		FilePersistenceDao filePersistenceDao = new FilePersistenceDao();

		try {
			if (filePersistenceDao.isConfiguredAuthPropertiesFile() == false) {
				AuthSettingsUI authSettingsWindow = new AuthSettingsUI();
				authSettingsWindow.setVisible(true);
				LOGGER.config("Authentication Settings applied successfully!");
			}
		} catch (FileNotFoundException e) {
			LOGGER.severe("Auth Properties not found attempting to setup authentication settings. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					null,
					"N�o foi poss�vel localizar o arquivo de propriedades para configurar a autentica��o no servidor!",
					"Configura��o do Sistema",
					JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			LOGGER.severe("Output input error attempting to setup authentication settings. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					null,
					"N�o foi poss�vel acessar o arquivo de propriedades para configurar a autentica��o no servidor!",
					"Configura��o do Sistema",
					JOptionPane.ERROR_MESSAGE);
		}

		try {
			if (filePersistenceDao.isConfiguredSystemPropertiesFile() == false) {
				SystemSettingsUI systemSettingsWindow = new SystemSettingsUI();
				systemSettingsWindow.setVisible(true);
				systemSettingsWindow.dispose();
				LOGGER.config("System Settings applied successfully!");
			}
		} catch (FileNotFoundException e) {
			LOGGER.severe("System Properties not found attempting to setup system settings. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					null,
					"N�o foi poss�vel localizar o arquivo de propriedades para configurar o sistema!",
					"Configura��o do Sistema",
					JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			LOGGER.severe("Output input error attempting to setup system settings. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					null,
					"N�o foi poss�vel acessar o arquivo de propriedades para configurar o sistema!",
					"Configura��o do Sistema",
					JOptionPane.ERROR_MESSAGE);
		}

		try {
			watchindDir = SystemUtilitiesMethods.doMakeDir();
		} catch (DirectoryNotEmptyException e) {
			LOGGER.severe("Attempting to create the directory synchronization and its subdirectories. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane.showMessageDialog(null, e.getMessage(),
					"Erro ao tentar criar os diret�rios",
					JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		} catch (SecurityException e) {
			LOGGER.severe("Attempting to create the directory synchronization and its subdirectories with no permission to. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
			JOptionPane
			.showMessageDialog(
					null,
					"Os diret�rios n�o foram criados corretamente pois n�o havia permiss�o para isto!\n"
							+ "Causa do erro: " + e.getMessage(),
							"Erro ao tentar criar os diret�rios",
							JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}


		SystemUtilitiesMethods.doShowTrayIcon();

		try {
			Path dir = Paths.get(watchindDir);
			WatchDirService watchDirService = new WatchDirService(dir, false);
			watchDirService.processEvents();
		} catch (IOException e) {
			LOGGER.severe("Attempting to watching for changes and events on watched path. Error: "
					+ e.getClass().getName() + ": " + e.getMessage());
		}
	}
}