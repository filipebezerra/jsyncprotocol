package br.com.protocolovirtual.jsyncprotocol.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Classe Respons�vel por realizar a comunica��o externa com o SGBD retornando
 * uma conex�o para efetuar transa��es com o banco de dados em caso de sucesso,
 * ou ao ocorrer uma exce��o imediatamento � relan�ado ao chamador do classe.
 * Esta classe cont�m um m�todo para realiza��o de teste de conex�o.
 * 
 */
public class ConnectionFactory {
	/**
	 * Link de conex�o com o banco de dados
	 */
	private static String URL;

	/**
	 * Servidor ou m�quina onde a conex�o ser� realizada
	 */
	private static String HOST;

	/**
	 * Porta de conex�o com servidor de banco de dados
	 */
	private static int PORT;

	/**
	 * Nome do banco de dados
	 */
	private static String DATABASE_NAME;

	/**
	 * Usu�rio que cont�m permiss�es de acesso e manipula��o no banco de dados
	 */
	private static String USERNAME;

	/**
	 * Senha do usu�rio que conectar� e efetuar� transa��es no banco de dados
	 */
	private static String PASSWORD;

	/**
	 * M�todo respons�vel por realizar conex�o direta com o banco de dados e
	 * retornar esta conex�o.
	 * 
	 * @return Uma conex�o direta com o banco de dados
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws GeneralSecurityException
	 *             Todas as classes abaixo s�o especializa��es desta.
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidAlgorithmParameterException
	 */
	public static Connection getConnection() throws IOException,
			ClassNotFoundException, SQLException, GeneralSecurityException {
		Connection connection = null;
		Properties authProperties = new FilePersistenceDao()
				.getAuthPropertiesDecryptedFile();
		Properties jdbcProperties = new FilePersistenceDao()
				.getJdbcPropertiesFile();
		Class.forName(jdbcProperties.getProperty("driver_url"));
		HOST = authProperties.getProperty("srvdb_host");
		PORT = Integer.parseInt(authProperties.getProperty("srvdb_port"));
		DATABASE_NAME = authProperties.getProperty("srvdb_dbname");
		USERNAME = authProperties.getProperty("srvdb_user");
		PASSWORD = authProperties.getProperty("srvdb_pw");
		URL = jdbcProperties.getProperty("connection_url") + HOST + ":"
				+ String.valueOf(PORT) + "/" + DATABASE_NAME;
		connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		return connection;
	}

	/**
	 * M�todo respons�vel por realizar uma conex�o de teste
	 * 
	 * @param host
	 * @param database
	 * @param port
	 * @param user
	 * @param pw
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void doTestConnection(final String host,
			final String database, final String port, final String user,
			final String pw) throws FileNotFoundException, IOException,
			ClassNotFoundException, SQLException {
		Properties jdbcProperties = new FilePersistenceDao()
				.getJdbcPropertiesFile();
		Class.forName(jdbcProperties.getProperty("driver_url"));
		String url = jdbcProperties.getProperty("connection_url") + host + ":"
				+ port + "/" + database;
		try (Connection connection = DriverManager.getConnection(url, user, pw)) {

		} catch (SQLException e) {
			throw e;
		}
	}

	/**
	 * 
	 * @param connection
	 * @param statement
	 * @param resultSet
	 * @throws SQLException
	 */
	public static void closeAll(Connection connection,
			PreparedStatement statement, ResultSet resultSet)
			throws SQLException {
		if (connection != null) {
			connection.close();
		}

		if (statement != null) {
			statement.close();
		}

		if (resultSet != null) {
			resultSet.close();
		}
	}
}
