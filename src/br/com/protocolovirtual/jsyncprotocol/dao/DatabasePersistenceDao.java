package br.com.protocolovirtual.jsyncprotocol.dao;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.protocolovirtual.jsyncprotocol.model.Obligation;
import br.com.protocolovirtual.jsyncprotocol.model.Office;
import br.com.protocolovirtual.jsyncprotocol.model.UserReceiver;
import br.com.protocolovirtual.jsyncprotocol.model.UserSender;

/**
 * Classe respons�vel por opera��es de recupera��o e inser��o de dados no banco
 * de dados. Os dados inseridos s�o referentes aos protocolos
 */
public class DatabasePersistenceDao {
	/**
	 * Constante que armazena o comando para recuperar o usu�rio que ser�
	 * respons�vel por enviar os protocolos.
	 */
	private static final String SELECT_USER_SENDER = "SELECT cod_usuario, cod_escritorio, "
			+ "nome, (SELECT nome FROM escritorios e WHERE e.cod_escritorio=u.cod_escritorio) "
			+ "as escritorio FROM usuarios u WHERE (usuario=? OR email=?) AND senha=sha1(?) "
			+ "AND excluido='0' AND lower(tipo_usuario)='funcionario_escritorio'";

	/**
	 * Constante que armazena o comando para recuperar o(s) usu�rio(s) que
	 * ser�(�o) o(s) receptor(es) do protocolo enviado.
	 */
	private static final String SELECT_USER_RECEIVER = "SELECT cod_usuario, nome FROM "
			+ "usuarios WHERE lower(tipo_usuario)='cliente' AND excluido='0' "
			+ "AND cod_escritorio=? AND lower(status)='ativo'";

	/**
	 * Constante que armazena o comando para recuperar a(s) obriga��o(�es)
	 * determinante(s) do protocolo enviado.
	 */
	private static final String SELECT_FIELD_OBRIGACOES = "SELECT cod_obrigacao, "
			+ "(SELECT nome FROM obrigacoes_nomes o_n WHERE o_n.cod = o.cod_nome) AS nome "
			+ "FROM obrigacoes AS o WHERE cod_escritorio=? AND excluido='0'";

	/**
	 * Constante que armazena o comando para inserir um registro na tabela de
	 * protocolos. Este registro � o protocolo enviado.
	 */
	private static final String INSERT_INTO_PROTOCOLOS = "INSERT into protocolos "
			+ "(cod_escritorio, remetente, nome_remetente, destinatario, tipos, "
			+ "cod_obrigacao, protocolo, anexo, descricao, dt_cadastro, excluido, "
			+ "tipo_envio, assunto, email_2, dt_finalizacao, usuario_finalizacao, "
			+ "nome_funcionario_finalizacao, anexo_finalizacao, dt_alteracao) "
			+ "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '0', 'escrit�rio para cliente', "
			+ "'', '', '', 0, '', '', '')";

	/**
	 * M�todo respons�vel por recuperar uma representa��o do usu�rio da tabela
	 * usuarios do banco de dados e armazen�-lo em uma inst�ncia da classe
	 * {@link UserSender}
	 * 
	 * @param userName
	 *            o nome de usu�rio ou email
	 * @param pw
	 *            a senha do usu�rio
	 * @return uma inst�ncia de uma usu�rio contendo seu c�digo, nome de
	 *         usu�rio, nome real e escrit�rio
	 * @throws SQLException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws GeneralSecurityException
	 * @throws Exception
	 */
	public UserSender retrieveUserSender(final String userName, final String pw)
			throws Exception, ClassNotFoundException, IOException,
			SQLException, GeneralSecurityException {

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		UserSender sender = null;

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(SELECT_USER_SENDER);
			statement.setObject(1, userName);
			statement.setObject(2, userName);
			statement.setObject(3, pw);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				sender = new UserSender();
				sender.setId(resultSet.getInt("cod_usuario"));
				sender.setName(resultSet.getString("nome"));
				sender.setOffice(new Office(resultSet.getInt("cod_escritorio"),
						resultSet.getString("escritorio")));
				sender.setUserName(userName);
			}
		} finally {
			ConnectionFactory.closeAll(connection, statement, resultSet);
		}

		return sender;
	}

	/**
	 * 
	 * @param OfficeId
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws SQLException
	 * @throws GeneralSecurityException
	 */
	public List<UserReceiver> retrieveUserReceiver(final int OfficeId)
			throws Exception, ClassNotFoundException, IOException,
			SQLException, GeneralSecurityException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<UserReceiver> listUserReceiver = new ArrayList<>();

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(SELECT_USER_RECEIVER);
			statement.setObject(1, OfficeId);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				UserReceiver userReceiver = new UserReceiver();
				userReceiver.setId(resultSet.getInt("cod_usuario"));
				userReceiver.setName(resultSet.getString("nome"));
				listUserReceiver.add(userReceiver);
			}
		} finally {
			ConnectionFactory.closeAll(connection, statement, resultSet);
		}

		return listUserReceiver;
	}

	/**
	 * 
	 * @param OfficeId
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws SQLException
	 * @throws GeneralSecurityException
	 */
	public List<Obligation> retrieveFieldObrigacao(final int OfficeId)
			throws ClassNotFoundException, IOException, SQLException,
			GeneralSecurityException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Obligation> listFieldObrigacao = new ArrayList<>();

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(SELECT_FIELD_OBRIGACOES);
			statement.setObject(1, OfficeId);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Obligation fieldObrigacao = new Obligation();
				fieldObrigacao.setId(resultSet.getInt("cod_obrigacao"));
				fieldObrigacao.setName(resultSet.getString("nome"));
				listFieldObrigacao.add(fieldObrigacao);
			}
		} finally {
			ConnectionFactory.closeAll(connection, statement, resultSet);
		}

		return listFieldObrigacao;
	}

	/**
	 * 
	 * @param params
	 *            Os par�metros esperados pelo comando de inser��o. S�o eles:
	 *            <b>cod_escritorio, remetente, nome_remetente, destinatario,
	 *            tipos, cod_obrigacao, protocolo, anexo, descricao,
	 *            dt_cadastro</b>.
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public void saveRecordFromProtocolSent(String[] params)
			throws ClassNotFoundException, IOException, SQLException,
			GeneralSecurityException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(INSERT_INTO_PROTOCOLOS);
			statement.setInt(1, Integer.valueOf(params[0]));
			statement.setInt(2, Integer.valueOf(params[1]));
			statement.setString(3, params[2]);
			statement.setInt(4, Integer.valueOf(params[3]));
			statement.setString(5, params[4]);
			statement.setInt(6, Integer.valueOf(params[5]));
			statement.setString(7, params[6]);
			statement.setString(8, params[7]);
			statement.setString(9, params[8]);
			statement.setString(10, params[9]);
			statement.executeUpdate();
		} finally {
			ConnectionFactory.closeAll(connection, statement, resultSet);
		}
	}
}
