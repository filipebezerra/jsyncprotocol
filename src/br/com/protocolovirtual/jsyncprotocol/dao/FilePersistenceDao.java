package br.com.protocolovirtual.jsyncprotocol.dao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import br.com.protocolovirtual.jsyncprotocol.security.EncryptorDES;
import br.com.protocolovirtual.jsyncprotocol.util.SystemUtilitiesConstants;

/**
 * Classe respons�vel por realizar opera��es de leitura e escrita nos arquivos
 * de propriedades do sistema. Estes arquivos constituem arquivos
 * {@link Properties}, no formato pares chave/valor. Atualmente os arquivos
 * manipulados s�o:
 * <em><b>system-conf.properties, auth-conf.properties e jdbc-conf.properties<b></em>
 * .
 * 
 */
public class FilePersistenceDao {

	/**
	 * M�todo gen�rico respons�vel por recuperar uma inst�ncia de um arquivo de
	 * propriedades do sistema.
	 * 
	 * @param propertiesFileConstantName
	 *            Constante que armazena estaticamente o caminho f�sico do
	 *            arquivo a ser recuperado.
	 * @return Um arquivo de propriedades
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	private Properties getPropertiesFile(final String propertiesFileConstantName)
			throws FileNotFoundException, IOException {
		FileInputStream in = null;
		Properties properties = null;
		try {
			properties = new Properties();
			in = new FileInputStream(propertiesFileConstantName);
			properties.load(in);
		} finally {
			if (in != null) {
				in.close();
			}
		}
		return properties;
	}

	/**
	 * M�todo gen�rico respons�vel por recuperar as propriedades de um arquivo
	 * de propriedades do sistema, informando se est� devidamente configurado.
	 * 
	 * @param propertiesFileConstantName
	 *            Constante que armazena estaticamente o caminho f�sico do
	 *            arquivo a ser recuperado.
	 * @return <b>True</b> se o arquivo estiver corretamente configurado, caso
	 *         contr�rio <b>False</b>
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	private boolean isConfiguredPropertiesFile(
			final String propertiesFileConstantName)
			throws FileNotFoundException, IOException {
		Properties properties = getPropertiesFile(propertiesFileConstantName);
		if (properties.isEmpty())
			return false;

		for (String key : properties.stringPropertyNames()) {
			if (properties.getProperty(key) == null
					|| properties.getProperty(key).isEmpty())
				return false;
		}
		return true;
	}

	/**
	 * M�todo respons�vel por recuperar uma inst�ncia do arquivo de configura��o
	 * de autentica��o do sistema. Autentica��o com servidor de banco de dados e
	 * do sistema de arquivos por meio do protocolo SSH. As propriedades s�o
	 * recuperadas criptografadas.
	 * 
	 * @return Arquivo de propriedades de autentica��o do sistema
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	public Properties getAuthPropertiesFile() throws FileNotFoundException,
			IOException {
		return getPropertiesFile(SystemUtilitiesConstants.RES_PROPERTIES_AUTH_PATH);
	}

	/**
	 * M�todo respons�vel por recuperar uma inst�ncia do arquivo de configura��o
	 * de autentica��o do sistema com os dados descriptografados. Autentica��o
	 * com servidor de banco de dados e do sistema de arquivos por meio do
	 * protocolo SSH. As propriedades s�o recuperadas descriptografadas.
	 * 
	 * @return Arquivo de propriedades de autentica��o do sistema
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 * @throws GeneralSecurityException
	 *             Todas as classes abaixo s�o especializa��es desta.
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidAlgorithmParameterException
	 */
	public Properties getAuthPropertiesDecryptedFile()
			throws FileNotFoundException, IOException, GeneralSecurityException {
		Properties tempProperties = getPropertiesFile(SystemUtilitiesConstants.RES_PROPERTIES_AUTH_PATH);
		Properties decryptedProperties = new Properties();
		for (String propertyName : tempProperties.stringPropertyNames()) {
			decryptedProperties.setProperty(propertyName, EncryptorDES
					.decrypt(tempProperties.getProperty(propertyName)));
		}
		return decryptedProperties;
	}

	/**
	 * M�todo respons�vel por aplicar as configura��es das propriedades do
	 * arquivo de configura��o de autentica��o do sistema. As propriedades s�o
	 * armazenadas com criptografia DES.
	 * 
	 * @param keysValues
	 *            Pares chave/valor contendo cada chave e seu respectivo valor
	 *            do arquivo de propriedades.
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 * @throws GeneralSecurityException
	 *             Todas as classes abaixo s�o especializa��es desta.
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidAlgorithmParameterException
	 */
	public void setAuthPropertiesFile(final Map<String, String> keysValues)
			throws FileNotFoundException, IOException, GeneralSecurityException {
		FileOutputStream out = null;
		Properties properties = getAuthPropertiesFile();
		for (Map.Entry<String, String> keyValue : keysValues.entrySet()) {
			properties.setProperty(keyValue.getKey(),
					EncryptorDES.encrypt(keyValue.getValue()));
		}
		try {
			out = new FileOutputStream(
					SystemUtilitiesConstants.RES_PROPERTIES_AUTH_PATH);
			properties.store(out, "Configura��es aplicadas");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * M�todo respons�vel por desfazer os valores das propriedades do arquivo de
	 * configura��o de autentica��o do sistema.
	 * 
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	public void emptyAuthPropertiesFile() throws FileNotFoundException,
			IOException {
		FileOutputStream out = null;
		Properties newProperties = getAuthPropertiesFile();
		for (String propertyName : newProperties.stringPropertyNames()) {
			newProperties.setProperty(propertyName, "");
		}
		try {
			out = new FileOutputStream(
					SystemUtilitiesConstants.RES_PROPERTIES_AUTH_PATH);
			newProperties.store(out, "Configura��es desfeitas");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * M�todo respons�vel por recuperar uma inst�ncia do arquivo de configura��o
	 * do jdbc (java database connectivity).
	 * 
	 * @return Inst�ncia do arquivo de configura��o do jdbc com suas
	 *         propriedades.
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	public Properties getJdbcPropertiesFile() throws FileNotFoundException,
			IOException {
		return getPropertiesFile(SystemUtilitiesConstants.RES_PROPERTIES_JDBC_PATH);
	}

	/**
	 * M�todo respons�vel por aplicar as configura��es das propriedades do
	 * arquivo de configura��o do jdbc (java database connectivity).
	 * 
	 * @param keysValues
	 *            Pares chave/valor contendo cada chave e seu respectivo valor
	 *            do arquivo de propriedades.
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	public void setJdbcPropertiesFile(final Map<String, String> keysValues)
			throws FileNotFoundException, IOException {
		FileOutputStream out = null;
		Properties properties = getJdbcPropertiesFile();
		for (Map.Entry<String, String> keyValue : keysValues.entrySet()) {
			properties.setProperty(keyValue.getKey(), keyValue.getValue());
		}
		try {
			out = new FileOutputStream(
					SystemUtilitiesConstants.RES_PROPERTIES_JDBC_PATH);
			properties.store(out, "Configura��es aplicadas");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * M�todo respons�vel por recuperar uma inst�ncia do arquivo de configura��o
	 * do sistema. Arquivo que cont�m o diret�rio a ser monitorado e o usu�rio
	 * logado.
	 * 
	 * @return Inst�ncia do arquivo de configura��o do sistema com suas
	 *         propriedades.
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	public Properties getSystemPropertiesFile() throws FileNotFoundException,
			IOException {
		return getPropertiesFile(SystemUtilitiesConstants.RES_PROPERTIES_SYSTEM_PATH);
	}

	/**
	 * M�todo respons�vel por aplicar as configura��es das propriedades do
	 * arquivo de configura��o do sistema.
	 * 
	 * @param keysValues
	 *            Pares chave/valor contendo cada chave e seu respectivo valor
	 *            do arquivo de propriedades.
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	public void setSystemPropertiesFile(final Map<String, String> keysValues)
			throws FileNotFoundException, IOException {
		FileOutputStream out = null;
		Properties properties = getSystemPropertiesFile();
		for (Map.Entry<String, String> keyValue : keysValues.entrySet()) {
			properties.setProperty(keyValue.getKey(), keyValue.getValue());
		}
		try {
			out = new FileOutputStream(
					SystemUtilitiesConstants.RES_PROPERTIES_SYSTEM_PATH);
			properties.store(out, "Configura��es aplicadas");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * M�todo respons�vel por desfazer os valores das propriedades do arquivo de
	 * configura��o do sistema.
	 * 
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	public void emptySystemPropertiesFile() throws FileNotFoundException,
			IOException {
		FileOutputStream out = null;
		Properties newProperties = getSystemPropertiesFile();
		for (String propertyName : newProperties.stringPropertyNames()) {
			newProperties.setProperty(propertyName, "");
		}
		try {
			out = new FileOutputStream(
					SystemUtilitiesConstants.RES_PROPERTIES_SYSTEM_PATH);
			newProperties.store(out, "Configura��es desfeitas");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * M�todo respons�vel por verificar se o arquivo de propriedades de
	 * configura��o de autentica��o do sistema est� corretamente configurado.
	 * 
	 * @param propertiesFileConstantName
	 *            Constante que armazena estaticamente o caminho f�sico do
	 *            arquivo a ser recuperado.
	 * @return <b>True</b> se o arquivo estiver corretamente configurado, caso
	 *         contr�rio <b>False</b>
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	public boolean isConfiguredAuthPropertiesFile()
			throws FileNotFoundException, IOException {
		return isConfiguredPropertiesFile(SystemUtilitiesConstants.RES_PROPERTIES_AUTH_PATH);
	}

	/**
	 * M�todo respons�vel por verificar se o arquivo de propriedades de
	 * configura��o do jdbc (java database connectivity) est� corretamente
	 * configurado.
	 * 
	 * @param propertiesFileConstantName
	 *            Constante que armazena estaticamente o caminho f�sico do
	 *            arquivo a ser recuperado.
	 * @return <b>True</b> se o arquivo estiver corretamente configurado, caso
	 *         contr�rio <b>False</b>
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	public boolean isConfiguredJdbcPropertiesFile()
			throws FileNotFoundException, IOException {
		return isConfiguredPropertiesFile(SystemUtilitiesConstants.RES_PROPERTIES_JDBC_PATH);
	}

	/**
	 * M�todo respons�vel por verificar se o arquivo de propriedades de
	 * configura��o do sistema est� corretamente configurado.
	 * 
	 * @param propertiesFileConstantName
	 *            Constante que armazena estaticamente o caminho f�sico do
	 *            arquivo a ser recuperado.
	 * @return <b>True</b> se o arquivo estiver corretamente configurado, caso
	 *         contr�rio <b>False</b>
	 * @throws FileNotFoundException
	 *             Erro ao buscar e n�o localizar o arquivo
	 * @throws IOException
	 *             Erro de acesso de leitura ou escrita no arquivo
	 */
	public boolean isConfiguredSystemPropertiesFile()
			throws FileNotFoundException, IOException {
		return isConfiguredPropertiesFile(SystemUtilitiesConstants.RES_PROPERTIES_SYSTEM_PATH);
	}
}
